#!/usr/bin/env python3

import requests
from requests.api import request
from requests.models import HTTPBasicAuth
import urllib3
import json
from base64 import b64encode

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

url_base = "https://api.crowdstrike.com"
header = {'accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
client_id = "adf97596e0024d9d9ed4af1a29c1b7b8"
secret = "HKJ7Q5ae3bw8lCc4I1XPTZ6RYrEjd2ShFu90tNUz"

# Encode id and secret for later
client_and_secret = client_id + ":" + secret
encoded = b64encode(bytes(client_and_secret, encoding='ascii')).decode('ascii')

# Login
print("--------------------")
print("Login to get a token")
url = url_base + "/oauth2/token"
payload = {'client_id': client_id, 'client_secret': secret}
x = requests.post(url, headers=header, data=payload)
if x.status_code == 201:
    token = x.json()['access_token']
    print("Token Assigned")
else:
    print("Something went wrong.", x.status_code, x.text)
    exit()

# Updated header for interacting with objects
auth = "Bearer " + token
header2 = {'accept': 'application/json', 'authorization': auth}
header3 = {'accept': 'application/json', 'authorization': auth, 'Content-Type': 'application/json'}

# List Hosts
print("----------")
print("List Hosts")
url = url_base + "/devices/queries/devices/v1"
x = requests.get(url, headers=header2)
if x.status_code == 200:
    for item in x.json()['resources']:
        url = url_base + "/devices/entities/devices/v1?ids=" + item
        x = requests.get(url, headers=header2)
        if x.status_code == 200:
            print("Host: ", x.json()['resources'][0]['hostname'])
        else:
            print("Something went wrong.", x.status_code, x.text)
else:
    print("Something went wrong.", x.status_code, x.text)

# List Groups
def list_groups():
    print("-----------")
    print("List Groups")
    url = url_base + "/devices/queries/host-groups/v1"
    x = requests.get(url, headers=header2)
    if x.status_code == 200:
        for group in x.json()['resources']:
            print("Group:")
            print(group)
            # Get Group Details
            url = url_base + "/devices/entities/host-groups/v1?ids=" + group
            x = requests.get(url, headers=header2)
            if x.status_code == 200:
                print("Name: ", x.json()['resources'][0]['name'], " Type: ", x.json()['resources'][0]['group_type'])
            else:
                print("Something went wrong.", x.status_code, x.text)
            # Get Group Members
            url = url_base + "/devices/queries/host-group-members/v1?id=" + group
            x = requests.get(url, headers=header2)
            if x.status_code == 200:
                print("Members:")
                for member in x.json()['resources']:
                    print(member)
            else:
                print("Something went wrong.", x.status_code, x.text)
    else:
        print("Something went wrong.", x.status_code, x.text)
    return
list_groups()


a = input("Press ENTER to continue")

# Create a Group
print("--------------")
print("Create a Group")
url = url_base + "/devices/entities/host-groups/v1"
payload = {'resources': [{'description': 'dragos test group 2', 'group_type': 'static', 'name': 'dragos_group2'}]}
x = requests.post(url, headers=header3, json=payload)
if x.status_code == 201:
    group_id = x.json()['resources'][0]['id']
    print(group_id)
    # Add members to the group
    url = url_base + "/devices/entities/host-group-actions/v1?action_name=add-hosts"
    payload = {'action_parameters': [{'name': 'filter', 'value': "device_id:['164dfac1a443466eb76dfa32b66bbb44']"}], 'ids': [ group_id ]}
    x = requests.post(url, headers=header3, json=payload)
    if x.status_code == 200:
        print(x.json())
    else:
        print("Something went wrong.", x.status_code, x.text)
else:
    print("Something went wrong.", x.status_code, x.text)
list_groups()

a = input("Press ENTER to continue")

# Delete a Group
print("--------------")
print("Delete a Group")
url = url_base + "/devices/entities/host-groups/v1?ids=" + group_id
x = requests.delete(url, headers=header2)
if x.status_code == 200:
    print("Deleted Group")
else:
    print("Something went wrong.", x.status_code, x.text)
list_groups()

a = input("Press ENTER to continue")

# List Detections
print("---------------")
print("List Detections")
url = url_base + "/detects/queries/detects/v1"
x = requests.get(url, headers=header2)
if x.status_code == 200:
    #print(x.json()['resources'])
    for detect in x.json()['resources']:
        url = url_base + "/detects/entities/summaries/GET/v1"
        payload = {'ids': [ detect ]}
        x = requests.post(url, headers=header3, json=payload)
        if x.status_code == 200:
            print("Detection ID: ", detect, "Hostname: ", x.json()['resources'][0]['device']['hostname'], 'Technique: ', x.json()['resources'][0]['behaviors'][0]['technique'], 'Description: ', x.json()['resources'][0]['behaviors'][0]['description'], 'IOC Value: ', x.json()['resources'][0]['behaviors'][0]['ioc_value'])
        else:
            print("Something went wrong.", x.status_code, x.text)
else:
    print("Something went wrong.", x.status_code, x.text)

a = input("Press ENTER to continue")

# List IOCs
def list_ioc():
    print("---------")
    print("List IOCs")
    url = url_base + "/iocs/queries/indicators/v1?filter=type:'domain'"
    x = requests.get(url, headers=header2)
    if x.status_code == 200:
        for ioc in x.json()['resources']:
            url = url_base + "/iocs/entities/indicators/v1?ids=" + ioc
            x = requests.get(url, headers=header2)
            if x.status_code == 200:
                print("ID: ", x.json()['resources'][0]['id'], "Type: ", x.json()['resources'][0]['type'], "Value: ", x.json()['resources'][0]['value'])
            else:
                print("Something went wrong.", x.status_code, x.text)
    else:
        print("Something went wrong.", x.status_code, x.text)
    return
list_ioc()

# Create an IOC
print("-------------")
print("Create an IOC")
url = url_base + "/iocs/entities/indicators/v1"
payload = { 'indicators': [{'action': 'detect', 'applied_globally': True, 'description': 'dragos IOC', 'platforms': ['windows', 'mac', 'linux'], 'severity': 'low', 'type': 'domain', 'value': 'dragos.com' }]}
x = requests.post(url, headers=header3, json=payload)
if x.status_code == 201:
    print("IOC added sucessfully")
    ioc_id = x.json()['resources'][0]['id']
    print(ioc_id)
else:
    print("Something went wrong.", x.status_code, x.text)
list_ioc()

a = input("Press ENTER to continue")

# Delete IOC
print("--------------")
print("Delete the IOC")
url = url_base + "/iocs/entities/indicators/v1?ids=" + ioc_id
x = requests.delete(url, headers=header2)
if x.status_code == 200:
    print("Successfully deleted IOC ", ioc_id)
else:
    print("Something went wrong.", x.status_code, x.text)
list_ioc()


# Logout
print("-----------------------")
print("Revoke the access token")
auth = "Basic " + encoded
header2 = header
header2['Authorization'] = auth
url = url_base + "/oauth2/revoke"
payload = {'token': token}
x = requests.post(url, headers=header2, data=payload)
if x.status_code == 200:
    print("Access token has been revoked")
else:
    print("Something went wrong.", x.status_code, x.text)
