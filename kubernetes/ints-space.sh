#!/bin/bash

## Get space usage about Integrations pods

for pod in $(kubectl get pods -A | grep integration | awk '{print $2}');
do
echo "---------------------"
echo "POD: $pod"
namespace=$(kubectl get pods -A | grep $pod | awk '{print $1}')
echo "NAMESPACE: $namespace"
container_id=$(kubectl get pod $pod -n $namespace -o json | jq -r .status.containerStatuses[].containerID | awk -F/ '{print $NF}')
echo "CONTAINER ID: $container_id"
disk_use=$(find / -type d -name "$container_id" 2>/dev/null | xargs du -ch | grep total | awk '{print $1}')
echo "DISK USAGE: $disk_use"
image=$(kubectl get pod $pod -n $namespace -o json | jq .spec.containers[].image)
echo "IMAGE: $image"
image_size_bytes=$(kubectl get nodes -o json | jq ".items[].status.images[] | select(.names[] | contains($image)) | .sizeBytes")
image_size_mb=$(($image_size_bytes / 1024 / 1024))
echo "Image Size: $image_size_mb MB"
echo "----------------------"
done
