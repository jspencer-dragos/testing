#!/usr/bin/env python3

## Find old artifacts

import requests
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
parser.add_argument("--delete", help="Delete the file", action="store_true")
args = parser.parse_args()

nexus_uri = "https://nexus.dragos.services/service/rest/v1/components"
repository = "docker-private"
older_than_days = 730

ago = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=older_than_days)

url = nexus_uri + "?repository=" + repository
x = requests.get(url, auth=(args.username, args.password))

if x.status_code == 200:
    continueToken = x.json()['continuationToken']
    item_count = len(x.json()['items'])
else:
    print("Connection Failed")
found = 0
total = 0
page = 1
f = open("findings.csv", "w")
#f.write("Component, Version, Last Modified, Last Downloaded \n")
f.close()
while continueToken != None and x.status_code == 200 and item_count > 0:
    for item in x.json()['items']:
        total += 1
        last_mod = datetime.datetime.strptime((item['assets'][0]['lastModified']).split("T")[0], '%Y-%m-%d')
        if item['assets'][0]['lastDownloaded'] == None:
            last_down = datetime.datetime.strptime('2000-01-01', '%Y-%m-%d')
        else:
            last_down = datetime.datetime.strptime((item['assets'][0]['lastDownloaded']).split("T")[0], '%Y-%m-%d')
        if last_mod < ago and last_down < ago:
            found += 1
            output = item['id'] + ", " + item['name'] + ", " + item['version'] + ", " + str(last_mod).split(" ")[0] + ", " + str(last_down).split(" ")[0]
            if args.delete == True:
                print("Deleting", item['name'], item['version'])
                url2 = nexus_uri + "/" + item['id']
                x2 = requests.delete(url2, auth=(args.username, args.password))
                if x2.status_code == 204:
                    print("Deleted Successfully")
                    output = output + "DELETED"
                else:
                    print("Something wet wrong", x2.status_code)
            print(output)
            print("-------------------------------------------------------------")
            f = open("findings.csv", "a")
            f.write(output + "\n")
            f.close()

    url = nexus_uri + "?continuationToken=" + continueToken + "&repository=" + repository
    x = requests.get(url, auth=(args.username, args.password))
    if x.status_code == 200:
        continueToken = x.json()['continuationToken']
        item_count = len(x.json()['items'])
        #print(x.json())
    else:
        print("Connection Failed")
        continueToken = None
    #continueToken =  None
    print("Items Found:", found)
    print("Total Items:", total)
    print("Page:", page)
    page += 1
