#!/usr/bin/env python3

## Find Docker artifacts by component name

in_file = "components-platform-content.txt"
out_file = "components-platform-content.csv"

import requests
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
parser.add_argument("--repository", help="Docker Repository Name", required=False, default="docker-private")
args = parser.parse_args()

nexus_uri = "https://nexus.dragos.services/service/rest/v1/search"

f = open(out_file, "w")
f.write("Component, Version, Last Modified \n")
f.close()

component_file = open(in_file, 'r')
lines = component_file.readlines()
for component in lines:
    url = nexus_uri + "?format=docker&repository=" + str(args.repository) + "&docker.imageName=" + str(component)
    x = requests.get(url, auth=(args.username, args.password))

    if x.status_code == 200:
        continueToken = x.json()['continuationToken']
    else:
        print("Connection Failed")
        continueToken = None

    while continueToken != None:
        for item in x.json()['items']:
            #print(item)
            last_mod = datetime.datetime.strptime((item['assets'][0]['lastModified']).split("T")[0], '%Y-%m-%d')
            output = item['name'] + ", " + item['version'] + ", " + str(last_mod).split(" ")[0]
            print(output)
            print("------------------------------------")
            f = open(out_file, "a")
            f.write(output + "\n")
            f.close()

        url = nexus_uri + "?continuationToken=" + continueToken + "&format=docker&repository=" + str(args.repository) + "&docker.imageName=" + str(component)
        x = requests.get(url, auth=(args.username, args.password))
        if x.status_code == 200:
            continueToken = x.json()['continuationToken']
            item_count = len(x.json()['items'])
            #print(x.json())
        else:
            print("Connection Failed")
            continueToken = None

        #continueToken = None