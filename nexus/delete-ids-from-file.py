#!/usr/bin/env python3

## Delete docker components by ID in file

import requests
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
parser.add_argument("--file", help="File of IDs", required=False, default="search-results.csv")
args = parser.parse_args()

nexus_uri = "https://nexus.dragos.services/service/rest/v1/components"

log_file = "delete-log.csv"

with open(args.file) as input_file:
    for line in input_file:
        print(line)
        id = line.split(",")[0].strip()
        url = nexus_uri + "/" + id
        x2 = requests.delete(url, auth=(args.username, args.password))
        if x2.status_code == 204:
            print("Deleted Successfully")
            f = open(log_file, "a")
            f.write(line)
            f.close()
        else:
            print("Something wet wrong", x2.status_code)

