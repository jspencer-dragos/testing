#!/usr/bin/env python3

import os
import sys
import requests
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
args = parser.parse_args()

nexus_search_uri = "https://nexus.dragos.services/service/rest/v1/search?"
nexus_repo = "dragos-2.x-dev"

url = nexus_search_uri + "repository=" + nexus_repo + "&name=*.dpp"
x = requests.get(url, auth=(args.username, args.password))

if x.status_code == 200:
    continueToken = x.json()['continuationToken']
else:
    print("Connection Failed")
    continueToken = None
    sys.exit()

f = open("nexus-dpps.csv", "w")
f.write("URL, Size \n")
f.close()

while continueToken != None:
    for item in x.json()['items']:
        #print(item)
        id = item['id']
        download_url = item['assets'][0]['downloadUrl']
        print(download_url)
        item_head = requests.head(download_url, auth=(args.username, args.password))
        if item_head.status_code == 200:
            size_bytes = int((json.loads(json.dumps(dict(item_head.headers))))['Content-Length'])
            size_gbytes = round((((size_bytes / 1024) / 1024) / 1024), 2)
        print(size_gbytes)
        print("-------------------------------------------------------")

        output = download_url + ", " + str(size_gbytes)
        f = open("nexus-dpps.csv", "a")
        f.write(output + "\n")
        f.close()


    url = nexus_search_uri + "continuationToken=" + continueToken + "&repository=" + nexus_repo + "&name=*.dpp"
    x = requests.get(url, auth=(args.username, args.password))
    if x.status_code == 200:
        continueToken = x.json()['continuationToken']
    else:
        print("Connection Failed")
        continueToken = None
