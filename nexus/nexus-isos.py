#!/usr/bin/env python3

import os
import sys
import requests
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
args = parser.parse_args()

nexus_search_uri = "https://nexus.dragos.services/service/rest/v1/search?"
nexus_repo = "dragos-2.x-dev"

url = nexus_search_uri + "repository=" + nexus_repo + "&name=*.iso"
x = requests.get(url, auth=(args.username, args.password))

if x.status_code == 200:
    continueToken = x.json()['continuationToken']
else:
    print("Connection Failed")
    continueToken = None
    sys.exit()

f = open("nexus-isos.csv", "w")
f.write("URL, Size, Last Updated, Last Downloaded, Checksum \n")
f.close()

while continueToken != None:
    for item in x.json()['items']:
        print(item)
        id = item['id']
        download_url = item['assets'][0]['downloadUrl']
        checksum = item['assets'][0]['checksum']['md5']
        last_mod = item['assets'][0]['lastModified']
        last_mod_day = last_mod.split('T')[0]
        print(download_url)
        print(last_mod)
        print(last_mod_day)
        item_head = requests.head(download_url, auth=(args.username, args.password))
        if item_head.status_code == 200:
            size_bytes = int((json.loads(json.dumps(dict(item_head.headers))))['Content-Length'])
            size_gbytes = round((((size_bytes / 1024) / 1024) / 1024), 2)
        print(size_gbytes)
        last_down_url = "https://nexus.dragos.services/service/rest/v1/components/" + id
        down_request = requests.get(last_down_url, auth=(args.username, args.password))
        if down_request.status_code == 200:
            last_down = down_request.json()['assets'][0]['lastDownloaded']
            last_down_day = last_down.split('T')[0]
        else:
            last_down = "1900-01-01"
        print(last_down)
        print(last_down_day)
        print("-------------------------------------------------------")

        output = download_url + ", " + str(size_gbytes) + ", " + last_mod_day + ", " + last_down_day + ", " + checksum
        f = open("nexus-isos.csv", "a")
        f.write(output + "\n")
        f.close()


    url = nexus_search_uri + "continuationToken=" + continueToken + "&repository=" + nexus_repo + "&name=*.iso"
    x = requests.get(url, auth=(args.username, args.password))
    if x.status_code == 200:
        continueToken = x.json()['continuationToken']
    else:
        print("Connection Failed")
        continueToken = None
    #continueToken = None
