#!/usr/bin/env python3

## Find Docker artifacts by component name or image tag

import requests
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
parser.add_argument("--repository", help="Docker Repository Name", required=False, default="docker-private")
parser.add_argument("--component", help="Docker Component Name", required=False, default="")
parser.add_argument("--tag", help="Docker Component Tag", required=False, default="")
parser.add_argument("--delete", help="Delete the file", action="store_true") # not implemented
args = parser.parse_args()

nexus_uri = "https://nexus.dragos.services/service/rest/v1/search"
nexus_uri2 = "Https://nexus.dragos.services/service/rest/v1/components"

url = nexus_uri + "?format=docker&repository=" + str(args.repository) + "&docker.imageName=" + str(args.component) + "&docker.imageTag=" + str(args.tag)
x = requests.get(url, auth=(args.username, args.password))

if x.status_code == 200:
    continueToken = x.json()['continuationToken']
else:
    print("Connection Failed")
    continueToken = None

f = open("search-results.csv", "w")
#f.write("Component, Version, Last Modified \n")
f.close()

while continueToken != None:
    for item in x.json()['items']:
        #print(item)
        last_mod = datetime.datetime.strptime((item['assets'][0]['lastModified']).split("T")[0], '%Y-%m-%d')
        output = item['id'] + " , " + item['name'] + ", " + item['version'] + ", " + str(last_mod).split(" ")[0]
        print(output)
        print("------------------------------------")
        f = open("search-results.csv", "a")
        f.write(output + "\n")
        f.close()

        ## Delete the item if --delete is used. 
        if args.delete:
            print("Deleting", item['name'], item['version'], item['id'])
            url2 = nexus_uri2 + "/" + item['id']
            x2 = requests.delete(url2, auth=(args.username, args.password))
            if x2.status_code == 204:
                print("Deleted Successfully")
            else:
                print("Something wet wrong", x2.status_code)

    url = nexus_uri + "?continuationToken=" + continueToken + "&format=docker&repository=" + str(args.repository) + "&docker.imageName=" + str(args.component)  + "&docker.imageTag=" + str(args.tag)
    x = requests.get(url, auth=(args.username, args.password))
    if x.status_code == 200:
        continueToken = x.json()['continuationToken']
        item_count = len(x.json()['items'])
        #print(x.json())
    else:
        print("Connection Failed")
        continueToken = None

    #continueToken = None