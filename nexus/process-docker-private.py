#!/usr/bin/env python3

## Process the CSV from docker-private-all.py

import pandas as pd
import datetime
from datetime import date


today_dash = date.today()
today = (date.today()).strftime("%Y%m%d")
in_file = today + '-docker-private.csv'
out_file = today + '-docker-private-processed.csv'
owners_file = 'owners.txt'

days_30 = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=30)
days_60 = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=60)
days_90 = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=90)
days_180 = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=180)
days_365 = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=365)
days_547 = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=547)
days_730 = datetime.datetime.combine(date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=730)

## Red the owners file to a list
owners = pd.read_csv(owners_file)

## Read component tags file to a Panda DataFrame
data = pd.read_csv(in_file)
data_sorted = data.sort_values('component')

## Print the first 5 rows of the DataFrame
#print(data.head())

## Print the DataFrame keys
print(data.keys())

## Count tags
#print(len(data))

## Create the output file
f = open(out_file, "w")
f.write("Total component tags: " + str(len(data)) + "\n\n")
f.write("component, owner, total tags, 30 days old, 60 days old, 90 days old, 180 days old, 1 yr old, 1.5 yrs old, 2 yrs old, notes \n")
f.close()

## Loop through unique component names
for comp in data_sorted['component'].unique():
    ## If there's an owner, add it
    if (owners['component'].eq(comp)).any():
        owner = owners[owners['component']==comp]['owner'].item()
        if pd.isna(owners[owners['component']==comp]['notes'].item()):
            notes = ""
        else:
            notes = owners[owners['component']==comp]['notes'].item()
    else:
        owner = 'TBD'
        f = open(owners_file, 'a')
        f.write(comp + ",TBD,\n")
        f.close()
    
    name = comp

    ## Print the items from the DataFrame that match the component. 
    print(data[data.component == comp])

    ## Does the item match the component
    

    ## Count the total components
    total = len(data[data['component']==comp])
    print(total)

    ## The count of DataFrame items that match the component & who's last_mod date is less than or equal to x days. (Watch for spaces in index names)
    total_30 = len(data[(data['component'] == comp) & (pd.to_datetime(data['last_mod']) <= str(days_30))])
    total_60 = len(data[(data['component'] == comp) & (pd.to_datetime(data['last_mod']) <= str(days_60))])
    total_90 = len(data[(data['component'] == comp) & (pd.to_datetime(data['last_mod']) <= str(days_90))])
    total_180 = len(data[(data['component'] == comp) & (pd.to_datetime(data['last_mod']) <= str(days_180))])
    total_365 = len(data[(data['component'] == comp) & (pd.to_datetime(data['last_mod']) <= str(days_365))])
    total_547 = len(data[(data['component'] == comp) & (pd.to_datetime(data['last_mod']) <= str(days_547))])
    total_730 = len(data[(data['component'] == comp) & (pd.to_datetime(data['last_mod']) <= str(days_730))])
    output = name + "," + str(owner) + "," + str(total) + "," + str(total_30) + "," + str(total_60) + "," + str(total_90) + "," + str(total_180) + "," + str(total_365) + "," + str(total_547) + "," + str(total_730) + "," + str(notes)
    print(output)
    f = open(out_file, "a")
    f.write(output + "\n")
    f.close()
    #break
