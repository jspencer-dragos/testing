#!/usr/bin/env python3

## Get a list of all docker-private artifacts

import requests
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
args = parser.parse_args()

nexus_uri = "https://nexus.dragos.services/service/rest/v1/components"
repository = "docker-private"

today = (date.today()).strftime("%Y%m%d")
out_file = today + '-docker-private.csv'

url = nexus_uri + "?repository=" + repository
x = requests.get(url, auth=(args.username, args.password))

if x.status_code == 200:
    continueToken = x.json()['continuationToken']
    item_count = len(x.json()['items'])
else:
    print("Connection Failed")
found = 0
total = 0
page = 1
f = open(out_file, "w")
f.write("component,version,last_mod,last_down \n")
f.close()
while continueToken != None and x.status_code == 200 and item_count > 0:
    for item in x.json()['items']:
        total += 1
        last_mod = datetime.datetime.strptime((item['assets'][0]['lastModified']).split("T")[0], '%Y-%m-%d')
        if item['assets'][0]['lastDownloaded'] == None:
            last_down = datetime.datetime.strptime('2000-01-01', '%Y-%m-%d')
        else:
            last_down = datetime.datetime.strptime((item['assets'][0]['lastDownloaded']).split("T")[0], '%Y-%m-%d')
        output = item['name'] + ", " + item['version'] + ", " + str(last_mod).split(" ")[0] + ", " + str(last_down).split(" ")[0]
        print(output)
        #print("-------------------------------------------------------------")
        f = open(out_file, "a")
        f.write(output + "\n")
        f.close()

    url = nexus_uri + "?continuationToken=" + continueToken + "&repository=" + repository
    x = requests.get(url, auth=(args.username, args.password))
    if x.status_code == 200:
        continueToken = x.json()['continuationToken']
        item_count = len(x.json()['items'])
        #print(x.json())
    else:
        print("Connection Failed")
        continueToken = None
    #continueToken =  None
    print("Total Items:", total)
    print("Page:", page)
    page += 1
