#!/usr/bin/env python3

import sys
import requests
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nexus Username", required=True)
parser.add_argument("--password", help="Nexus Password", required=True)
args = parser.parse_args()

nexus_uri = "https://nexus.dragos.services/service/rest/v1/components"
repository = "docker-private"

