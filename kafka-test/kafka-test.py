#!/usr/bin/env python3

import os
import ssl
import datetime

from kafka import KafkaConsumer, KafkaProducer
from kafka.structs import TopicPartition


now = datetime.datetime.now()
topic = "JamesTest"
message = "test message " + str(now)
location = os.path.dirname(__file__)
client_cert = os.path.join(location, 'client.pem')
client_key = os.path.join(location, 'client.key')
ssl_ca = os.path.join(location, 'RootCA.pem')

context = ssl.create_default_context()
context.load_cert_chain(certfile=client_cert, keyfile=client_key)
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE

print(message)

#producer = KafkaProducer(bootstrap_servers="10.0.50.179:9092")
#producer.send(topic, bytes(message, encoding='utf8'))
#producer.flush()

consumer = KafkaConsumer(bootstrap_servers="10.0.50.179:9093", 
                            consumer_timeout_ms=1000,
                            security_protocol="SSL",
                            ssl_context=context,
                            ssl_check_hostname=False,
                            ssl_cafile=ssl_ca,
                            ssl_certfile=client_cert,
                            ssl_keyfile=client_key
                            )
if consumer.bootstrap_connected():
    print("connected")
else:
    print("not connected")
consumer.assign([TopicPartition(topic, 0)])
print(consumer.assignment())

consumer.seek_to_beginning()

for msg in consumer:
    print(msg)

consumer.close()
