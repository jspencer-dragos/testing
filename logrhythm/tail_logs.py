#!/usr/bin/eng python3

## Tail LogRhythm syslog log file over SMB.

import tempfile
from smb.SMBConnection import SMBConnection

username = "administrator"
password = "Dr@gosSyst3m"
client_name = "LWD172401"
host_name = "WIN-F7S61NOTBT7"
host_ip = "10.0.50.125"
folder = "logrhythm-logs"
file = "syslogfile.log"

conn = SMBConnection(username=username, password=password, my_name=client_name, remote_name=host_name)

conn.connect(ip=host_ip)

file_obj = tempfile.NamedTemporaryFile()
file_attributes, filesize = conn.retrieveFile(service_name=folder, path=file, file_obj=file_obj)
file_obj.seek(0)
print(file_obj.read())
file_obj.close()

conn.close()