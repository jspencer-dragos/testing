#!/usr/bin/env python3

## Connect to the Dragos WorldView API and pull stix2

import json
import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

api_url = "https://portal.dragos.com:443/api/v1/indicators/stix2"
api_token = "22D48CD01DF6FB16CDD02585"
api_secret = "GhuMrjtt38vHPqE/pAdSc2lGqy4="

req_header = {
    "API-Token": api_token,
    "API-Secret": api_secret
}

api_req = requests.get(api_url, headers=req_header)
api_data = json.loads(api_req.text)

#print(json.dumps(api_data, indent=4))

with open("worldview-indicators.stix2", "w") as file:
    file.write(json.dumps(api_data, indent=2))