#!/usr/bin/env python3

## Parse Worldview Indicators

import json
import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

page_size = 1000
types = ["domain", "ip"]
api_token = "22D48CD01DF6FB16CDD02585"
api_secret = "GhuMrjtt38vHPqE/pAdSc2lGqy4="
out_path = "/var/www/html/"

req_header = {
    "API-Token": api_token,
    "API-Secret": api_secret
}

for type in types:
    page = 1
    pages = 1
    file_name = "dragos-worldview-indicators-" + type + ".txt"
    header = "# Dragos Worldview API Indicators - " + type + "\n"
    with open(file_name, 'w') as file:
        #file.write(header)
        pass
    while (page <= pages):
        api_url = "https://portal.dragos.com:443/api/v1/indicators?page_size=" + str(page_size) + "&type=" + type + "&page=" + str(page)
        api_req = requests.get(api_url, headers=req_header)
        api_data = json.loads(api_req.text)
        pages = api_data["total_pages"]
        for item in api_data["indicators"]:
            value = item["value"]
            print(value)
            output = value + "\n"
            with open(file_name, 'a') as file:
                file.write(output)
        print("page " + str(page) + " of " + str(pages))
        page = page + 1

