#!/usr/bin/env python3

## File upload example

import requests

url = "http://httpbin.org/post"

files = {'file': open('test-indicator-ips.txt', 'rb')}

r = requests.post(url, files=files)

print(r, r.text)