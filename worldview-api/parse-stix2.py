#!/usr/bin/env python3

## Read stix2 file and parse the data

import json

filename = "worldview-indicators.stix2"
file = open(filename)
data = json.load(file)

ip_array = []
domain_array = []

for item in data["objects"]:
    if item["type"] == "indicator":
        value_type = (((item["pattern"]).replace('[','')).split(':'))[0]
        value = (((((item["pattern"]).replace(']','')).split('='))[1]).replace('\'','')).strip()
        #print(value_type, value)
        if value_type == "domain-name":
            domain_array.append(value)
        elif value_type == "ipv4-addr":
            ip_array.append(value)

print(ip_array)
