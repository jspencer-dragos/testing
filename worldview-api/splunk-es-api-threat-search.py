## curl -v -k -u admin:Dr@gosSyst3m https://10.20.8.13:8089/services/data/threat_intel/item/ip_intel -d item='{"ip":"66.203.125.238"}' -G -X GET

import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

server_ip = "10.20.8.13"
user_name = "admin"
pass_word = "Dr@gosSyst3m"

api_url = "https://" + server_ip + ':8089/services/data/threat_intel/item/ip_intel?item={"ip":"101.248.240.199"}'

api_req = requests.get(api_url, auth=(user_name, pass_word), verify=False)

print(api_req, api_req.text)