#!/usr/bin/env python3

## Upload to the Splunk ES API

#### !!!! BROKEN

import base64
import json
import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

server_ip = "10.20.8.13"
user_name = "admin"
pass_word = "Dr@gosSyst3m"
file_name = "test-indicator-ips.csv"
group = "dragos_test_ips"
category = "dragos"

api_url = "https://" + server_ip + ":8089/services/data/threat_intel/upload"

with open(file_name, 'rb') as file:
    file_encoded = base64.b64encode(file.read())

#print(file_encoded)
#print(file_encoded.decode('utf-8'))

api_data = {
    "file_name": file_name,
    "encoded_content": file_encoded.decode('utf-8'),
    "weight": "20",
    "threat_category": category,
    "threat_group": group,
    "overwrite": "true"
}
#print(api_data)
api_data_json = json.dumps(api_data)
print(api_data_json)

api_req = requests.post(api_url, auth=(user_name, pass_word), data=api_data, verify=False)
print(api_req)

#with open("output.html", 'w') as output:
#    output.write(api_req.text)