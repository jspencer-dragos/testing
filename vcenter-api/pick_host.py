#!/usr/bin/env python3

### Script to pick the best host

## imports
import sys
import operator
import argparse
from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim

## Arguments
parser = argparse.ArgumentParser()
parser.add_argument('--username', help='vCenter Username', required=True)
parser.add_argument('--password', help='vCenter Password', required=True)
parser.add_argument('--vcenter', help='vCenter Hostname', required=False, default='eng-vcenter.hq.dragos.services')
parser.add_argument('-datacenter', help='vCenter Datacenter', required=False, default='HQ-Engineering')
parser.add_argument('--cluster', help='vCenter Cluster Name', required=False, default='DEV')
args=parser.parse_args()

## Variables
min_bytes = 42949672960 # 40GB

def best_host():
    ## Sub-functions
    # Function to disconnect
    def disconnect():
        try:
            Disconnect(c)
            return "Disconnected"
        except:
            return "Failed to Disconnect"

    # Function to lookup IDs from Names
    def helper(connection, vim_type, name, folder=None, recurse=True):
        """
        Search objects for the name and type specified.
        Sample:
        helper(c, [vim.Datastore], "HQ-Engineering")
        """
        if folder is None:
            folder = connection.content.rootFolder
        obj = None
        container = connection.content.viewManager.CreateContainerView(folder, vim_type, recurse)

        for object_ref in container.view:
            if object_ref.name == name:
                obj = object_ref
                break
        container.Destroy()
        return obj
    ## The work
    # Login to vCenter
    try:
        c = SmartConnect(host=args.vcenter, user=args.username, pwd=args.password, disableSslCertValidation=True)
        print("Successfully connected to ", args.vcenter)
    except:
        print("Connection failed to ", args.vcenter)
        sys.exit()

    # Name to ID conversions
    datacenter = helper(c, [vim.Datacenter], args.datacenter)
    cluster = helper(c, [vim.ClusterComputeResource], args.cluster)

    # Create a list of ESXi hosts in the cluster in the datacenter
    hosts=[]
    dc_view = c.content.viewManager.CreateContainerView(c.content.rootFolder, [vim.Datacenter], True)
    for dc in dc_view.view:
        if dc == datacenter:
            for entity in dc.hostFolder.childEntity:
                if entity == cluster:
                    for host in entity.host:
                        #print(host)
                        hosts.append(host)
    dc_view.Destroy()
    host_list = []
    for esxi in hosts:
        sitestores = 0
        sensors = 0
        # Count the SiteStore and Sensor VMs in each host
        for vm in esxi.vm:
            if 'sensor' in vm.name.lower():
                sensors += 1
            elif 'sitestore' in vm.name.lower():
                sitestores += 1

        # Pick the best local datatore per host. Exclude the 'logs' datastore
        store_list = []
        for store in esxi.datastore:
            if store.summary.multipleHostAccess == False and store.summary.accessible == True and store.summary.maintenanceMode == 'normal' and 'log' not in store.name and 'SSD' in store.name and store.summary.freeSpace > min_bytes:
                #print(esxi.name, store.name, store.summary)
                store_dict = {'store_name': store.name, 'free': store.summary.freeSpace}
                store_list.append(store_dict)
                sorted_store_list = sorted(store_list, key=operator.itemgetter('free'), reverse=True)
        #print(esxi.name, sorted_store_list)
        if store_list:
            host_dict = {'host_name': esxi.name, 'num_vm': len(list(esxi.vm)), 'sitestores': sitestores, 'sensors': sensors, 'datastore': sorted_store_list[0]['store_name'], 'free': sorted_store_list[0]['free']}
            host_list.append(host_dict)
    sorted_host_list = sorted(host_list, key=operator.itemgetter('num_vm', 'free', 'sensors', 'sitestores'))
    #print(sorted_host_list)

    # Disconnect
    dis = disconnect()
    print(dis)

    return [sorted_host_list[0]['host_name'], sorted_host_list[0]['datastore']]

result = best_host()
print(result)