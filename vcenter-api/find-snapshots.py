#!/usr/bin/env python3

## Find VM Snapshots

## References:
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/getallvms.py

# Requires pyvmomi

import sys
import argparse
from pyVim.connect import SmartConnect, Disconnect
from pyVim.task import WaitForTask
from pyVmomi import vim
from datetime import datetime

# Variables
vcenter = "eng-vcenter-vx.bwi.dragos.services"


# Argparser
parser = argparse.ArgumentParser()
parser.add_argument("--username", help="vCenter Username", required=True)
parser.add_argument("--password", help="vCenter Password", required=True)
args = parser.parse_args()


# Function to disconnect
def disconnect():
    try:
        Disconnect(c)
        return "Disconnected"
    except:
        return "Failed to Disconnect"
    
# Login to vCenter
try:
    c = SmartConnect(host=vcenter, user=args.username, pwd=args.password, disableSslCertValidation=True)
    print("Successfully connected")
except:
    print("Connection failed")
    sys.exit()

test = "ENGOPS-OPNLDAP1"

content = c.RetrieveContent()
container = content.rootFolder
view_type = [vim.VirtualMachine]
container_view = content.viewManager.CreateContainerView(container, view_type, True)
children = container_view.view
for child in children:
    if child.snapshot is not None and child.name == test:
        for snap in child.snapshot.rootSnapshotList:
            print(child.name, snap.name, snap.snapshot)
            WaitForTask(snap.snapshot.RemoveSnapshot_Task(True))




dis = disconnect()
print(dis)