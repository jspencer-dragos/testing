#!/usr/bin/env python3

import sys
import time
import argparse
#import pyVmomi
from pyVmomi import vim
from pyVim.connect import SmartConnect, Disconnect

vcenter = "eng-vcenter.hq.dragos.services"
datacenter_name = "Colo-Engineering"
cluster_name = "EngOps-Colo"
esxi_host_name = "eng-hv302.bwi.dragos.services"

parser = argparse.ArgumentParser()
parser.add_argument("--vcenter_username", help='vCenter Username', required=True)
parser.add_argument("--vcenter_password", help='vCenter Password', required=True)
args = parser.parse_args()

# Function to lookup IDs from Names
def helper(connection, vim_type, name, folder=None, recurse=True):
    """
    Search objects for the name and type specified.
    Sample:
    helper(c, [vim.Datastore], "HQ-Engineering")
    """
    if folder is None:
        folder = connection.content.rootFolder
    obj = None
    container = connection.content.viewManager.CreateContainerView(folder, vim_type, recurse)

    for object_ref in container.view:
        if object_ref.name == name:
            obj = object_ref
            break
    container.Destroy()
    return obj

# Function to disconnect
def disconnect():
    try:
        Disconnect(c)
        return "Disconnected"
    except:
        return "Failed to Disconnect"
    
# Login to vCenter
try:
    c = SmartConnect(host=vcenter, user=args.vcenter_username, pwd=args.vcenter_password, disableSslCertValidation=True)
    print("Successfully connected")
except:
    print("Connection failed")
    sys.exit()

# Convert names to IDs
datacenter = helper(c, [vim.Datacenter], datacenter_name)
cluster = helper(c, [vim.ClusterComputeResource], cluster_name)
esxi_host = helper(c, [vim.HostSystem], esxi_host_name)

# Rescan HBA
#esxi_host.configManager.storageSystem.RescanHba('vmhba64')

# Rescan All HBAs
esxi_host.configManager.storageSystem.RescanAllHba()

# Refresh Storage
esxi_host.configManager.storageSystem.RefreshStorageSystem()


## Disconnect
dis = disconnect()
print(dis)