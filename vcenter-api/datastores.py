#!/usr/bin/env python3

## This script is useless


# Login to vCenter API, List datastores
# vCenter API doesn't support uploading directly to a datstore. 

import getpass
import requests
import urllib3
import json
from base64 import b64encode

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

# Variables

vcenter = "https://eng-vcenter.hq.dragos.services"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")
datastore = "ISOs"

user_and_pass = user_name + ":" + pass_word
encode = b64encode(bytes(user_and_pass, encoding='ascii')).decode('ascii')

# Login and get a session ID
url = vcenter + "/api/session"
header = {'Authorization': 'Basic ' + encode}
x = requests.post(url, headers=header, verify=False)
if x.status_code == 201:
    session_id = x.json()
else:
    print("Something went wrong", x.status_code, x.text)
    exit

# List Datastores and get the datastore value of the datastore variable
url = vcenter + "/rest/vcenter/datastore"
header = {'vmware-api-session-id': session_id}
x = requests.get(url, headers=header, verify=False)
if x.status_code == 200:
    #print(x.json()['value'])
    for ds in x.json()['value']:
        print(ds['name'], ds['datastore'])

print("-----------------------------------")


# Get Details of a Datastore
url = vcenter + "/rest/vcenter/datastore?filter.names=" + datastore
header = {'vmware-api-session-id': session_id}
x = requests.get(url, headers=header, verify=False)
if x.status_code == 200:
    print(x.json()['value'])

# Close the session
url = vcenter + "/api/session"
header = {'vmware-api-session-id': session_id}
x = requests.delete(url, headers=header, verify=False)
if x.status_code == 204:
    print("Logged off successfully")