#!/usr/bin/env python3

# Using the SOAP API
# List datastore contents
# Upload a file to the datastore.
# https://eng-vcenter.hq.dragos.services/folder?dcPath=HQ-Engineering&dsName=ISOs

import getpass
import requests
from requests.auth import HTTPBasicAuth
import urllib3
from bs4 import BeautifulSoup

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

# Variables

vcenter = "https://eng-vcenter.hq.dragos.services"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")
datacenter = "HQ-Engineering"
datastore = "QE"
file = "./gparted-live.iso"


# List Datastore contents
def list_ds_contents():
    url = vcenter + "/folder?dcPath=" + datacenter + "&dsName=" + datastore
    x = requests.get(url, auth=HTTPBasicAuth(user_name, pass_word), verify=False)
    if x.status_code == 200:
        #print(x.cookies)
        soup = BeautifulSoup(x.text, 'html.parser')
        print(soup)
        table = soup.find_all('table')[1]
        for link in table.find_all('a'):
            print(link.text)
list_ds_contents()
    
# Upload File to Datastore
# test_file = open(file, "rb")
# file_name = file.split("/")[-1]
# url = vcenter + "/folder/" + file_name
# header = {'Content-Type' : 'application/octet-stream'}
# params = {'dsName': datastore, 'dcPath' : datacenter}
# x = requests.put(url, params=params, headers=header, auth=HTTPBasicAuth(user_name, pass_word), data=test_file, verify=False)
# print(x.status_code, x.text)


# list_ds_contents()