#!/usr/bin/env python3

# Clone a VM  and customize

# Requires pyvmomi

## To-Do:
# Make it standalone

# References:
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/tools/pchelper.py
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/clone_vm.py
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/add_nic_to_vm.py
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/delete_nic_from_vm.py
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/vm_power_on.py

from ipaddress import ip_address
import sys
import getpass
import time
from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim

# Variables
vcenter = "eng-vcenter.hq.dragos.services"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")

datacenter_name = "HQ-Engineering"
cluster_name = "EngOps"
esxi_host_name = "eng-hv15.hq.dragos.services"
folder_name = "EngOps"
datastore_name = "EngOps_HQ"
nic_num = 1
portgroup_name = "VLAN 2004 (EngOps)"
template_name = "JS-TEST"
hostname = "js-pyvm-test"
address = "10.20.4.11"
subnet = "255.255.255.0"
gateway = "10.20.4.1"
dns1 = "10.0.30.20"
dns2 = "10.0.30.21"


# Function to lookup IDs from Names
def helper(connection, vim_type, name, folder=None, recurse=True):
    """
    Search objects for the name and type specified.
    Sample:
    helper(c, [vim.Datastore], "HQ-Engineering")
    """
    if folder is None:
        folder = connection.content.rootFolder
    obj = None
    container = connection.content.viewManager.CreateContainerView(folder, vim_type, recurse)

    for object_ref in container.view:
        if object_ref.name == name:
            obj = object_ref
            break
    container.Destroy()
    return obj

# Function to disconnect
def disconnect():
    try:
        Disconnect(c)
        return "Disconnected"
    except:
        return "Failed to Disconnect"


# Login to vCenter
try:
    c = SmartConnect(host=vcenter, user=user_name, pwd=pass_word, disableSslCertValidation=True)
    print("Successfully connected")
except:
    print("Connection failed")
    sys.exit()

# Convert names to IDs
datacenter = helper(c, [vim.Datacenter], datacenter_name)
folder = helper(c, [vim.Folder], folder_name)
datastore = helper(c, [vim.Datastore], datastore_name)
cluster = helper(c, [vim.ClusterComputeResource], cluster_name)
esxi_host = helper(c, [vim.HostSystem], esxi_host_name)
template = helper(c, [vim.VirtualMachine], template_name)
network = helper(c, [vim.Network], portgroup_name)
print(datacenter, folder, datastore, cluster, esxi_host, template, network)

# Identity Settings (Required by Customization Spec)
ident = vim.vm.customization.LinuxPrep()
ident.domain = "dragos.services"
ident.hostName = vim.vm.customization.FixedName()
ident.hostName.name = hostname

# Adapter Map (Needed for Customization Spec. Not Required)
# Nic 1
adaptermap0 = vim.vm.customization.AdapterMapping()
adaptermap0.adapter = vim.vm.customization.IPSettings()
# Static IP
adaptermap0.adapter.ip = vim.vm.customization.FixedIp()
adaptermap0.adapter.ip.ipAddress = address
adaptermap0.adapter.subnetMask = subnet
adaptermap0.adapter.gateway = gateway
# Nic 2
adaptermap1 = vim.vm.customization.AdapterMapping()
adaptermap1.adapter = vim.vm.customization.IPSettings()
# DHCP
adaptermap1.adapter.ip = vim.vm.customization.DhcpIpGenerator()

# Customization Spec (Needed for Clone Spec. Not Required)
customspec = vim.vm.customization.Specification()
customspec.identity = ident
customspec.globalIPSettings = vim.vm.customization.GlobalIPSettings()
customspec.globalIPSettings.dnsServerList = [dns1, dns2]
customspec.nicSettingMap = [adaptermap0, adaptermap1]

# Relocation Spec (Required by Clone Spec)
relospec = vim.vm.RelocateSpec()
relospec.datastore = datastore
relospec.folder = folder
relospec.host = esxi_host

# Clone Spec (Reqiured by Clone)
clonespec = vim.vm.CloneSpec()
clonespec.customization = customspec
clonespec.location = relospec
clonespec.powerOn = False
clonespec.template = False

## Clone Template
try:
    task = template.Clone(folder=folder, name=hostname.upper(), spec=clonespec)
    while task.info.state == vim.TaskInfo.State.running:
        time.sleep(2)
    print("Template cloned")
except:
    print("Clone failed")
    dis = disconnect()
    print(dis)
    sys.exit()

# Find the VM that was just created
try:
    vm = helper(c, [vim.VirtualMachine], hostname.upper())
    if vm is None:
        raise ValueError()
    print(vm)
except:
    print("Unable to find VM")
    dis = disconnect()
    print(dis)
    sys.exit()

# Find NIC 1
nicprefix = "Network adapter "
niclabel = nicprefix + str(nic_num)
virtualnic = None
for dev in vm.config.hardware.device:
    if isinstance(dev, vim.vm.device.VirtualEthernetCard) and dev.deviceInfo.label == niclabel:
        virtualnic = dev
if not virtualnic:
    print("Unable to find NIC")
    dis = disconnect()
    print(dis)
    sys.exit()

# Set the VLAN
nicspec = vim.vm.device.VirtualDeviceSpec()
nicspec.operation = vim.vm.device.VirtualDeviceSpec.Operation.edit
nicspec.device = virtualnic
nicspec.device.backing.network = network
nicspec.device.backing.deviceName = portgroup_name
confspec = vim.vm.ConfigSpec()
confspec.deviceChange = [nicspec]
try:
    task = vm.ReconfigVM_Task(spec=confspec)
    while task.info.state == vim.TaskInfo.State.running:
        time.sleep(2)
    print("VM Updated")
except:
    print("VM Update Failed")
    dis = disconnect()
    print(dis)
    sys.exit()

## Power On
try:
    task = vm.PowerOn()
    while task.info.state == vim.TaskInfo.State.running:
        time.sleep(2)
    print("VM Powered On")
except:
    print("VM not powered on.")

## Disconnect
dis = disconnect()
print(dis)