#!/usr/bin/env python3

# Login to vCenter API and Create a VM

import getpass
import requests
import urllib3
import json
from base64 import b64encode

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

# Variables
vcenter = "https://eng-vcenter.hq.dragos.services"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")

# VM Variables
vm_name = "JS-API-TEST"
folder = "group-v16088" # EngOps
compute = "host-21607" # eng-hv15.hq.dragos.services
storage = "datastore-1001" # SysEng
os = "DEBIAN_10_64"
iso = "[ISOs] Linux/debian-10.10.0-amd64-netinst.iso"
firmware = "EFI"
cpu_count = 2
cores_per_cpu = 2
memory_mb = 8192
disk_size_bytes = 107374182400
storage_policy = "b6423670-8552-66e8-adc1-fa6ae01abeac" # Management Storage Policy - Thin
vlan = "network-14503" # VLAN 2004 (EngOps)
mac_type = "MANUAL"
mac_address = "00:50:56:84:d9:80"
false = False
true = True

# Encode the username and password
user_and_pass = user_name + ":" + pass_word
encode = b64encode(bytes(user_and_pass, encoding='ascii')).decode('ascii')


# Login and get a session ID
url = vcenter + "/api/session"
header = {'Authorization': 'Basic ' + encode}
x = requests.post(url, headers=header, verify=False)
if x.status_code == 201:
    print("Logged In")
    session_id = x.json()
else:
    print("Something went wrong", x.status_code, x.text)
    exit()


# Create a VM
print("Creating a VM")
url = vcenter + "/api/vcenter/vm"
header = {'vmware-api-session-id' : session_id, 'Content-Type' : 'application/json'}
data = {
    "name": vm_name,
	"guest_OS": os,
	"hardware_version": "VMX_19",
    "placement": {
		"datastore": storage,
		"folder": folder,
		"host": compute
	},
	"boot": {
		"type": firmware
	},
	"cpu": {
		"cores_per_socket": cores_per_cpu,
		"count": cpu_count
	},
	"memory": {
		"size_MiB": memory_mb
	},
	"cdroms": [
		{
			"backing": {
				"iso_file": iso,
				"type": "ISO_FILE"
			},
			"start_connected": true,
			"type": "IDE"
		}
	],
	"disks": [
		{
			"new_vmdk": {
				"capacity": disk_size_bytes,
                "storage_policy": {
                    "policy" : storage_policy
                }
			},
			"type": "SCSI"
		}
	],
	"nics": [
		{
			"backing": {
				"network": vlan,
				"type": "STANDARD_PORTGROUP"
			},
			"mac_address": mac_address,
			"mac_type": mac_type,
			"start_connected": true,
			"type": "VMXNET3"
		}
	]
}
x = requests.post(url, headers=header, data=json.dumps(data), verify=False)
print(x.status_code, x.text)


# Close the session
url = vcenter + "/api/session"
header = {'vmware-api-session-id': session_id}
x = requests.delete(url, headers=header, verify=False)
if x.status_code == 204:
    print("Logged off successfully")