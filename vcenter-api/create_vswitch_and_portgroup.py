#!/usr/bin/env python3

# Create a vswitch and portgroup on a vCenter Host

# Requires pyvmomi
# Reference: https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/add_vswitch_to_host.py
# Reference: https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/add_portgroup_to_vswitch.py

# Login to vCenter API

import sys
import re
import getpass
from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim


# Variables

vcenter = "eng-vcenter.hq.dragos.services"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")

vcenter_host = "eng-hv307"
vswitch_name = "content_hv307_pair_07"
vswitch_ports = 1024
vswitch_mtu = 9000
vswitch_security_promiscuous = True
vswitch_security_forged = True
vswitch_security_mac = True
portgroup_name = vswitch_name
portgroup_vlan = 4095



# Login
try:
    c = SmartConnect(host=vcenter, user=user_name, pwd=pass_word, disableSslCertValidation=True)
    print("Successfully connected")
except:
    print("Connection failed")
    sys.exit()

# Get the host ID
host_view = c.content.viewManager.CreateContainerView(c.content.rootFolder, [vim.HostSystem], True)
hosts = list(host_view.view)
match_obj = []
for esxi in hosts:
    if re.findall(r'%s.*' % vcenter_host, esxi.name):
        match_obj.append(esxi)
host = match_obj[0]
host_view.Destroy()
# print(host)

# vSwitch Spec
vswitch_spec = vim.host.VirtualSwitch.Specification()
vswitch_spec.numPorts = vswitch_ports
vswitch_spec.mtu = vswitch_mtu
vswitch_spec.policy = vim.host.NetworkPolicy()
vswitch_spec.policy.security = vim.host.NetworkPolicy.SecurityPolicy()
vswitch_spec.policy.security.allowPromiscuous = vswitch_security_promiscuous
vswitch_spec.policy.security.forgedTransmits = vswitch_security_forged
vswitch_spec.policy.security.macChanges = vswitch_security_mac
vswitch_spec.policy.offloadPolicy = vim.host.NetOffloadCapabilities()
vswitch_spec.policy.offloadPolicy.tcpSegmentation = True
vswitch_spec.policy.offloadPolicy.zeroCopyXmit = True
vswitch_spec.policy.offloadPolicy.csumOffload = True
vswitch_spec.policy.shapingPolicy = vim.host.NetworkPolicy.TrafficShapingPolicy()
vswitch_spec.policy.shapingPolicy.enabled = False
vswitch_spec.policy.nicTeaming = vim.host.NetworkPolicy.NicTeamingPolicy()
vswitch_spec.policy.nicTeaming.notifySwitches = True
vswitch_spec.policy.nicTeaming.rollingOrder = False
vswitch_spec.policy.nicTeaming.failureCriteria = vim.host.NetworkPolicy.NicFailureCriteria()
vswitch_spec.policy.nicTeaming.failureCriteria.fullDuplex = False
vswitch_spec.policy.nicTeaming.failureCriteria.percentage = 0
vswitch_spec.policy.nicTeaming.failureCriteria.checkErrorPercent = False
vswitch_spec.policy.nicTeaming.failureCriteria.checkDuplex = False
vswitch_spec.policy.nicTeaming.failureCriteria.checkBeacon = False
vswitch_spec.policy.nicTeaming.failureCriteria.speed = 10
vswitch_spec.policy.nicTeaming.failureCriteria.checkSpeed = 'minimum'
vswitch_spec.policy.nicTeaming.policy = 'loadbalance_srcid'
vswitch_spec.policy.nicTeaming.reversePolicy = True
#print(vswitch_spec)

# Create the vSwitch
try:
    host.configManager.networkSystem.AddVirtualSwitch(vswitch_name, vswitch_spec)
    print("vSwitch created")
except vim.fault.AlreadyExists:
    print("vSwitch already exists")
except:
    print("vSwitch creation failed")
    sys.exit()

# PortGroup Spec
portgroup_spec = vim.host.PortGroup.Specification()
portgroup_spec.vswitchName = vswitch_name
portgroup_spec.name = portgroup_name
portgroup_spec.vlanId = portgroup_vlan
portgroup_spec.policy = vim.host.NetworkPolicy()

# Create the portgroup
try:
    host.configManager.networkSystem.AddPortGroup(portgroup_spec)
    print("PortGroup Created")
except vim.fault.AlreadyExists:
    print("PortGroup already exists")
except:
    print("PortGroup creation failed")

# Close the session
Disconnect(c)