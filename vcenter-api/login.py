#!/usr/bin/env python3

# Login to vCenter API

import getpass
import requests
import urllib3
import json
from base64 import b64encode

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

# Variables

vcenter = "https://eng-vcenter.hq.dragos.services"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")

user_and_pass = user_name + ":" + pass_word
encode = b64encode(bytes(user_and_pass, encoding='ascii')).decode('ascii')

# Login and get a session ID
url = vcenter + "/api/session"
header = {'Authorization': 'Basic ' + encode}
x = requests.post(url, headers=header, verify=False)
if x.status_code == 201:
    session_id = x.json()
else:
    print("Something went wrong", x.status_code, x.text)
    exit



# Close the session
url = vcenter + "/api/session"
header = {'vmware-api-session-id': session_id}
x = requests.delete(url, headers=header, verify=False)
if x.status_code == 204:
    print("Logged off successfully")