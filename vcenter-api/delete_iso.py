#!/usr/bin/env python3

# Using the SOAP API
# List datastore contents
# Upload a file to the datastore.
# https://eng-vcenter.hq.dragos.services/folder?dcPath=HQ-Engineering&dsName=ISOs

import getpass
import requests
from requests.auth import HTTPBasicAuth
import urllib3
from bs4 import BeautifulSoup

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

# Variables

vcenter = "https://eng-vcenter.hq.dragos.services"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")
datacenter = "HQ-Engineering"
datastore = "ISOs"

    
# Delete ISO from Datastore
file_name = '202204131132-offline-DragOS-2.1.0-202204130200-amd64.iso'
url = vcenter + "/folder/" + file_name
header = {'Content-Type' : 'application/octet-stream'}
params = {'dsName': datastore, 'dcPath' : datacenter}
x = requests.delete(url, params=params, headers=header, auth=HTTPBasicAuth(user_name, pass_word), verify=False)
print(x.status_code, x.text)
