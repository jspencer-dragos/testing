## Query Redfish API

#!/usr/bin/env python3

import getpass
from webbrowser import get
import requests
import json
import urllib3
from base64 import b64encode

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

ilo = "10.20.0.101"
user_name = "Administrator"
pass_word = getpass.getpass("Password?")

user_and_pass = user_name + ":" + pass_word
encode = b64encode(bytes(user_and_pass, encoding='ascii')).decode('ascii')

url_base = "https://" + ilo + "/redfish/v1"

url = url_base + "/managers/1/snmpservice"
header = {'Content-Type': 'application/json'}
x = requests.get(url, headers=header, auth=(user_name, pass_word), verify=False)
if x.status_code == 200:
    print(x.json())
