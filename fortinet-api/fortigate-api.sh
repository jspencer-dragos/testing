#!/bin/bash

## FortiGate API Testing

read -p "FortiGate IP? " ip
read -p "API Username? " user
read -p "API Key? " key

# Get server cert
cert=$(echo | openssl s_client -showcerts -connect $ip:443 2>/dev/null | openssl x509 -inform pem)
echo "Server Cert"
echo "$cert"

# Test API Connection
echo "Test API Connection"
connect=$(curl -k "https://$ip/api/v2/cmdb/system/status/?access_token=$key"
echo "$connect"
