#!/usr/bin/env python3

import requests
import json
import urllib3
import ssl
import os

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

fortimanager = "10.20.8.51"
port = "443"
user_name = input("Username: ")
pass_word = input("Password: ")

url = "https://" + fortimanager + ":" + port + "/jsonrpc"

# Get and store the certificate
cert = ssl.get_server_certificate((fortimanager, port))
cert_file = open('cert.crt', 'w')
cert_file.write(cert)
cert_file.close()


# Login and get a Session ID
print("--------------------")
print("Getting a Session ID")
payload = { 'method': 'exec', 'params': [{'data':{'user': user_name, 'passwd': pass_word}, 'url': '/sys/login/user'}] }
x = requests.post(url, data=json.dumps(payload), verify='cert.crt')
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    ses_id = x.json()['session']
    print("Received a Session ID: ", ses_id)
else:
    print("Something went wrong", x.status_code, x.text)
    exit()


# List Address Objects in the Global ADOM that start with dragos.
def list_addresses():
    print("---------------")
    print("List Address Objects in the Global ADOM")
    uri = "/pm/config/global/obj/firewall/address/"
    payload = { 'method': 'get', 'params': [{ 'filter': [['type', '==', 0 ], '&&', ['name', 'like', 'dragos%' ]], 'url': uri }], 'session': ses_id }
    x = requests.post(url, data=json.dumps(payload), verify=False)
    if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
        for item in x.json()['result'][0]['data']:
            address_name = item['name']
            address_ip = item['subnet'][0]
            address_subnet = item['subnet'][1]
            print(address_name, address_ip, address_subnet)
    else:
        print("Something went wrong", x.status_code, x.text)
    return
list_addresses()


# List Address Groups in the Global ADOM
def list_address_groups():
    print("--------------------------------------")
    print("List Address Groups in the Global ADOM")
    uri = "/pm/config/global/obj/firewall/addrgrp"
    payload = { 'method': 'get', 'params': [{ 'filter': [[ 'name', 'like', 'dragos%' ]], 'url': uri }], 'session': ses_id }
    x = requests.post(url, data=json.dumps(payload), verify=False)
    if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
        for item in x.json()['result'][0]['data']:
            group_name = item['name']
            for member in item['member']:
                print(group_name, '-', member)
    else:
        print("Something went wrong", x.status_code, x.text)
    return
list_address_groups()


# Add an Address to the Global ADOM
print('--------------------------')
print('Add an Adress to the Global ADOM')
new_address_name = "dragos_address5"
uri = "/pm/config/global/obj/firewall/address/"
payload = { 'method': 'add', 'params': [{ 'data': [{ 'name': new_address_name, 'subnet': ['192.168.1.3', '255.255.255.255'] }], 'url': uri }], 'session': ses_id }
x = requests.post(url, data=json.dumps(payload), verify=False)
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    print("Address added")
else:
    print("Something went wrong", x.status_code, x.text)
list_addresses()

# Add an Address Group to the Global ADOM
print("---------------------------------------")
print("Add an Address Group to the Global ADOM")
new_address_group_name = "dragos_group3"
address_group_members = ['dragos_address4', 'dragos_address5']
uri = "/pm/config/global/obj/firewall/addrgrp"
payload = { 'method': 'add', 'params': [{ 'data': [{ 'name': new_address_group_name, 'member': address_group_members }], 'url': uri }], 'session': ses_id }
x = requests.post(url, data=json.dumps(payload), verify=False)
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    print("Added Address Group", new_address_group_name)
else:
    print("Something went wrong", x.status_code, x.text)
list_address_groups()

a = input("Press enter to continue")


# Delete an Address Group from the Global ADOM
print('-------------------------------')
print('Delete an Adress Group from the Global ADOM')
uri = "/pm/config/global/obj/firewall/addrgrp/" + new_address_group_name
payload = { 'method': 'delete', 'params': [{ 'url': uri }], 'session': ses_id }
x = requests.post(url, data=json.dumps(payload), verify=False)
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    print("Deleted Address Group", new_address_group_name)
else:
    print("Something went wrong", x.status_code, x.text)
list_address_groups()

# Delete an Address from the Global ADOM
print('-------------------------------')
print('Delete an Adress from the Global ADOM')
new_address_name = "dragos_address5"
uri = "/pm/config/global/obj/firewall/address/" + new_address_name
payload = { 'method': 'delete', 'params': [{ 'url': uri }], 'session': ses_id }
x = requests.post(url, data=json.dumps(payload), verify=False)
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    print("Address deleted")
else:
    print("Something went wrong", x.status_code, x.text)
list_addresses()


# Logout
print("-----------")
print("Logging Out")
payload = { 'method': 'exec', 'params': [{'url': '/sys/logout'}], 'session': ses_id}
x = requests.post(url, data=json.dumps(payload), verify=False)
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    print("Logged Out")
else:
    print("Something went wrong", x.status_code, x.text)


# Delete the cert file
os.remove("cert.crt")
