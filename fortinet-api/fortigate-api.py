#!/usr/bin/env python3

import requests
import json
from requests import api
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

fortigate_ip = "10.20.8.55"
fortigate_port = "443"
api_key = "pmrhHc9dppcrxHn7hb9mbfbHs6xqb7"

url_base = "https://" + fortigate_ip + ":" + fortigate_port
header = {'Content-Type':'application/json'}

# List Addresses
def list_addresses():
    print("-----------------------")
    print("List Addresses")
    url = url_base + "/api/v2/cmdb/firewall/address/?access_token=" + api_key
    response = requests.get(url, headers=header ,verify=False)
    print(response.status_code)
    if response.status_code == 200:
        for item in response.json()['results']:
            if item['type'] == 'ipmask' and item['name'].startswith("1A"):
                print(item['name'], item['subnet'])
    return
list_addresses()

# Add Address
print("---------------------")
print("Add an address object")
url = url_base + "/api/v2/cmdb/firewall/address/?access_token=" + api_key
payload = {'name':'1A-test2','subnet':'192.168.1.2 255.255.255.255','type':'ipmask'}
response = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(response.json()['status'], response.json()['mkey'])
list_addresses()

# Delete Address
print("------------------------")
print("Delete an Address Object")
url = url_base + "/api/v2/cmdb/firewall/address/1A-test2/?access_token=" + api_key
response = requests.delete(url, headers=header, verify=False)
print(response.json()['status'], response.json()['mkey'])
list_addresses()
