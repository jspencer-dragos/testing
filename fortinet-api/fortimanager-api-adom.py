#!/usr/bin/env python3

import requests
import json
import urllib3
import ssl
import os

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

fortimanager = "10.20.8.51"
port = "443"
user_name = input("Username: ")
pass_word = input("Password: ")

url = "https://" + fortimanager + ":" + port + "/jsonrpc"

# Get and store the certificate
cert = ssl.get_server_certificate((fortimanager, port))
cert_file = open('cert.crt', 'w')
cert_file.write(cert)
cert_file.close()


# Login and get a Session ID
print("--------------------")
print("Getting a Session ID")
payload = { 'method': 'exec', 'params': [{'data':{'user': user_name, 'passwd': pass_word}, 'url': '/sys/login/user'}] }
x = requests.post(url, data=json.dumps(payload), verify='cert.crt')
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    ses_id = x.json()['session']
    print("Received a Session ID: ", ses_id)
else:
    print("Something went wrong", x.status_code, x.text)
    exit()

# List the ADOMs that are FortiGate(1), Major Release 6, Minor Release 4, Mode 1,  and Enabled 
print("-----------------------")
print("Getting a list of ADOMs")
payload = { 'method': 'get', 'params': [{'filter':[['restricted_prds', '==', 1],'&&',['os_ver', '==', 6],'&&',['mr', '==', 4],'&&',['state', '==', 1],'&&',['mode', '==', 1]], 'url': '/dvmdb/adom'}], 'session': ses_id}
x = requests.post(url, data=json.dumps(payload), verify=False)
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    adoms = x.json()['result'][0]['data']
    for item in x.json()['result'][0]['data']:
        print(item['name'])
    parsed = json.loads(x.text)
    output = json.dumps(parsed, indent=4, sort_keys=True)
    test = open('adoms.txt', 'w')
    test.write(output)
    test.close()
else:
    print("Something went wrong", x.status_code, x.text)


# List Firewalls in the ADOMs
print("--------------")
print("Listing Firewalls in all ADOMs")
for adom in adoms:
    adom_name = adom['name']
    payload = { 'method': 'get', 'params': [{  'filter':[['mgt_vdom', '==', adom['name'] ]], 'url': '/dvmdb/device' }], 'session': ses_id }
    x = requests.post(url, data=json.dumps(payload), verify=False)
    if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
        firewalls = x.json()['result'][0]['data']
        for item in x.json()['result'][0]['data']:
            print(adom_name, item['hostname'], item['ip'])
        parsed = json.loads(x.text)
        output = json.dumps(parsed, indent=4, sort_keys=True)
        test = open('firewalls.txt', 'w')
        test.write(output)
        test.close()
    else:
        print("Something went wrong", x.status_code, x.text)

# Print Address Objects that are IP/Netmask in each ADOM
def list_addresses():
    print("---------------")
    print("Print Addres Objects that are IP/Netmask in each ADOM")
    for adom in adoms:
        adom_name = adom['name']
        uri = "/pm/config/adom/" + adom['name'] + "/obj/firewall/address/"
        payload = { 'method': 'get', 'params': [{ 'filter': [['type', '==', 0 ]], 'url': uri }], 'session': ses_id }
        x = requests.post(url, data=json.dumps(payload), verify=False)
        if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
            parsed = json.loads(x.text)
            output = json.dumps(parsed, indent=4, sort_keys=True)
            test = open('addresses.txt', 'w')
            test.write(output)
            test.close()
            for item in x.json()['result'][0]['data']:
                address_name = item['name']
                address_ip = item['subnet'][0]
                address_subnet = item['subnet'][1]
                print(adom_name, address_name, address_ip, address_subnet)
        else:
            print("Something went wrong", x.status_code, x.text)
    return
list_addresses()

# Add an Address to all ADOMs
print('--------------------------')
print('Add an Adress to all ADOMs')
new_address_name = "dragos_address3"
for adom in adoms:
    adom_name = adom['name']
    uri = "/pm/config/adom/" + adom['name'] + "/obj/firewall/address/"
    payload = { 'method': 'add', 'params': [{ 'data': [{ 'name': new_address_name, 'subnet': ['192.168.1.3', '255.255.255.255'] }], 'url': uri }], 'session': ses_id }
    x = requests.post(url, data=json.dumps(payload), verify=False)
    if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
        print("Address added")
    else:
        print("Something went wrong", x.status_code, x.text)

# Delete an Address from all ADOMs
print('-------------------------------')
print('Delete an Adress from all ADOMs')
new_address_name = "dragos_address3"
for adom in adoms:
    adom_name = adom['name']
    uri = "/pm/config/adom/" + adom['name'] + "/obj/firewall/address/" + new_address_name
    payload = { 'method': 'delete', 'params': [{ 'url': uri }], 'session': ses_id }
    x = requests.post(url, data=json.dumps(payload), verify=False)
    if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
        print("Address deleted")
    else:
        print("Something went wrong", x.status_code, x.text)

# Logout
print("-----------")
print("Logging Out")
payload = { 'method': 'exec', 'params': [{'url': '/sys/logout'}], 'session': ses_id}
x = requests.post(url, data=json.dumps(payload), verify=False)
if x.status_code == 200 and x.json()['result'][0]['status']['code'] == 0:
    print("Logged Out")
else:
    print("Something went wrong", x.status_code, x.text)


# Delete the cert file
os.remove("cert.crt")
