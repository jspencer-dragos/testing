#!/bin/bash

## Script to sign RPMs

read -p "GPG Key Password: " cert_pass
read -p "Signing Passphrase: " signing_pass

# Import the signing key if isn't already
if test -z "$(gpg -K | grep ^uid | grep 'Dragos Release Signing Key')"
then
    echo "Signing key is not imported"
    if ! test -e ./dragos_signing_priv.gpg
    then
        read -p "What the path to dragos_signing_priv.pgp? " privgpg
    else
        echo "Key file found."
        privgpg="./dragos_signing_priv.gpg"
    fi
    # Import the signing key
    gpg --batch --import --allow-secret-key-import --passphrase $cert_pass  $privgpg

else
    echo "GPG signing key exists"
fi

# Create the RPM macro file if it doesn't exist
if ! test -e ~/.rpmmacros
then
cat > ~/.rpmmacros <<EOL
%_signature gpg
%_gpg_path ~/.gnupg
%_gpg_name Dragos, INC. (Dragos Release Signing Key) <operations@dragos.com>
%__gpg /usr/bin/gpg
EOL
fi

read -p "Path to the rpm file: " rpmfile
if ! test -e $rpmfile
then
    echo "File does not exist"
    exit
fi

# Sign the RPM
gpg --export -a 5AE8C32A > /tmp/dragos_pub.key
rpm --import /tmp/dragos_pub.key
rpm --resign $rpmfile

# Verify the RPM
verify=$(rpm -Kv $rpmfile)
echo "$verify"
key_id=$(echo $verify  | grep "key ID" | tail -n 1 | awk -F'[,:]' '{print $2}')
echo "$key_id"

# Encrypt the RPM
gpg --batch --sign --symmetric --cipher-algo AES256 --passphrase $signing_pass $rpmfile


gpgfile="$rpmfile.gpg"
echo "Generating MD5 checksum"
md5sum $gpgfile
echo "Generating SHA256 checksum"
sha256sum $gpgfile

echo "Finished"