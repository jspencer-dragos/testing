#!/usr/bin/env python3

import argparse
from datetime import datetime
import requests
import sys
import xml.etree.ElementTree as ET

jenkins_url = "https://jenkins.cloud.dragos.services"

parser = argparse.ArgumentParser()
parser.add_argument('--username', help="Jenkins Username", required=True)
parser.add_argument('--password', help="Jenkins Password or Token", required=True)
args = parser.parse_args()

now = datetime.now()

url = jenkins_url + '/computer/api/xml?tree=computer[executors[currentExecutable[url]],oneOffExecutors[currentExecutable[url]]]&xpath=//url&wrapper=builds'
x = requests.get(url, auth=(args.username, args.password))
if x.status_code == 200:
    root = ET.fromstring(x.text)
    for child in root:
        #print(child.text)
        url = child.text + "/api/json"
        x = requests.get(url, auth=(args.username, args.password))
        if x.status_code == 200:
            print(x.json()['fullDisplayName'])
            timestamp = datetime.fromtimestamp(x.json()['timestamp'] / 1000.0)
            print(timestamp)
            diff = now - timestamp
            print(diff)
else:
    sys.exit()