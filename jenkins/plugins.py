#!/usr/bin/env python3

## Compare plugin versions

from jenkinsapi.jenkins import Jenkins
import urllib3
import getpass
import json

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

servers = ['cloud','test']
plugin_dict = {}

for server in servers:
    jenkins_url = 'https://jenkins.' + server + '.dragos.services' 
    user_name = input("Username for " + server + " : ")
    pass_word = getpass.getpass("Password or API Token for " + server + " : ")

    conn = Jenkins(jenkins_url, ssl_verify=False, username=user_name, password=pass_word)

    for plugin in conn.get_plugins().values():
        #print(plugin.shortName, plugin.version)
        if plugin.shortName not in plugin_dict:
            plugin_dict[plugin.shortName] = {}
            plugin_dict[plugin.shortName][server] = plugin.version
            plugin_dict[plugin.shortName]['url'] = plugin.url
            plugin_dict[plugin.shortName][server + '-enabled'] = plugin.enabled
        else:
            plugin_dict[plugin.shortName][server] = plugin.version
            plugin_dict[plugin.shortName][server + '-enabled'] = plugin.enabled

with open('jenkins_plugins.json', 'w') as convert_file:
    convert_file.write(json.dumps(plugin_dict))

print(plugin_dict)