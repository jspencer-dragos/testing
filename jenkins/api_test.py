#!/usr/bin/env python3

from jenkinsapi.jenkins import Jenkins
from jenkinsapi.node import Node


jenkins_url = "https://jenkins.cloud.dragos.services"
user_name = "jenkins_service@dragos.com"
api_token = input("API Token: ")

conn = Jenkins(jenkins_url, username=user_name, password=api_token)
jenkins_object = conn.get_jenkins_obj()
agent_dict = {}
for agent in conn.get_nodes().values():
    #print(agent)
    if "HQ" in str(agent):
        print(agent)
        agent_url = conn.get_node_url(nodename=str(agent))
        node_conn = Node(jenkins_object, agent_url, str(agent), agent_dict)
        print(node_conn.get_monitor())
        break
