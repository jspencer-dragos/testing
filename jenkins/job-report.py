#!/usr/bin/env python3

## Query Jenkins Jobs

import argparse
from jenkinsapi.jenkins import Jenkins

jenkins_url = "https://jenkins.cloud.dragos.services"

parser = argparse.ArgumentParser()
parser.add_argument('--username', help="Jenkins Username", required=True)
parser.add_argument('--password', help="Jenkins Password or Token", required=True)
args = parser.parse_args()

conn = Jenkins(jenkins_url, username=args.username, password=args.password)

for job in conn.get_jobs_info():
    url = str(job[0])
    name = str(job[1])
    job_info = conn.get_job_by_url(url, name)
    if job_info.is_enabled():
        build_info = job_info.get_last_build_or_none()
        if build_info is not None:
            build_data = job_info.get_build_metadata(build_info.get_number())
            running = build_data.is_running()
            status = build_data.get_status()
            try:
                agent = build_data.get_slave()
            except:
                agent = "None"
            #master = build_data.get_master_job()
            start_time = build_data.get_timestamp()
            duration = build_data.get_duration()
            print(name, running, status, agent, start_time, duration)
            try:
                print(build_data._data["culprits"][0]["fullName"])
            except:
                pass
            #break