#!/usr/bin/env python3

## Test AWS SMTP Relay using StartTLS

import ssl
import smtplib

host = "email-smtp.us-east-1.amazonaws.com"
port = "25"
username = "AKIAZ5WNI5BZEDZMV7VK"
password = "BHOo03Y9w+y9hRFGeJm6ZD/I3E1bdwAEkvqMYelqZs80"
sender = "engops@dragos.com"
recipient = "jspencer@dragos.com"
message = """\
    Subject: Test email
    Test message from Python."""

context = ssl.create_default_context()

try:
    server = smtplib.SMTP(host,port)
    server.ehlo() # Can be omitted
    server.starttls(context=context) # Secure the connection
    server.ehlo() # Can be omitted
    server.login(username, password)
    server.sendmail(sender, recipient, message)
except Exception as e:
    # Print any error messages to stdout
    print(e)
finally:
    server.quit() 