#!/usr/bin/env python3

api_token = '4d6268dcf955ab9a02262b574f3883fe'
temp_dir = '~/jaws_tmp'

import os
import argparse
import requests
import shutil
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parser = argparse.ArgumentParser()
parser.add_argument("--jaws_id", help='Comma Separated List of JAWS IDs', required=True)
parser.add_argument("--interface", help='Playback Interface Name', default='ens224')
parser.add_argument("--iterations", help="How many times do you want to repeat", default=1, type=int)
args = parser.parse_args()

iters = 1

# Create the temp directory if it doesn't exist
my_dir = os.path.expanduser(temp_dir)
if not os.path.exists(my_dir):
    os.makedirs(my_dir)

while(iters <= args.iterations):
    for id in list(args.jaws_id.split(',')):
        # Check if the file has been downloaded already and download
        file_path = my_dir + "/" + id + ".pcap"
        if not os.path.exists(file_path):
            print("Downloading", id,"...")
            url = 'https://jaws.dragos.services/api/v1/' + api_token + "/download/" + id
            with requests.get(url, verify=False, stream=True) as r:
                r.raise_for_status()
                with open(file_path, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        f.write(chunk)
            print("... Finished")
        # echo "tcpreplay ALL= NOPASSWD: /usr/bin/tcpreplay-edit" > /etc/sudoers.d/tcpreplay
        replay_command = "sudo tcpreplay-edit --stats=1 -M 100 -i " + args.interface + " --mtu-trunc -C " + file_path
        print(replay_command)
        os.system(replay_command)
    iters += 1

# Remove the temp directory and its contents
print("Cleaning up")
shutil.rmtree(my_dir)