#!/bin/bash

## Cisco ASA API Testing


#echo "List Network Objects using User Auth"
#test=$(curl --insecure --user username:password --header "User-Agent: REST API Agent" https://10.20.8.59/api/objects/networkobjects)
#echo "$test"

echo "Get a Token"
get_token=$(curl --insecure --head --silent \
	--request "POST" \
	--user username:password \
	--header "User-Agent: REST API Agent" \
	--header "Content-Type: application/json" \
	https://10.20.8.59/api/tokenservices)

response=$(echo "$get_token" | grep ^HTTP | awk '{print $2}')
if [ "$response" = "204" ]; then
	api_token=$(echo "$get_token" | grep ^X-Auth-Token | awk '{print $2}' | xargs)
else
	echo "ERROR"
	break
fi
token=$(echo $api_token)
echo "$token"


echo "Delete the Token"

del_token=$(curl --insecure --head \
	--request "DELETE" \
	--header "X-Auth-Token: $api_token" \
	--header "User-Agent: REST API Agent" \
	--header "Content-Type: application/json" \
	https://10.20.8.59/api/tokenservices/$api_token)

echo "$del_token"
#response=$(echo "$del_token" | grep ^HTTP | awk '{print $2}')
#if [ "$response" = "204" ]; then
#	echo "SUCCESS"
#else
#	echo "ERROR"
#fi
