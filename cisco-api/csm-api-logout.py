#!/usr/bin/env python3

import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

address = "10.20.8.60"
port = "443"
cookie = {'asCookie': '1118928366'}

url_base = "https://" + address + ":" + port + "/nbi/"

# Logout
print("------")
print("Logout")
url = url_base + "logout"
payload = """<?xml version="1.0" encoding="UTF-8"?>
<csm:logoutRequest xmlns:csm="csm">
<protVersion>1.0</protVersion>
<reqId>123</reqId>
</csm:logoutRequest>"""
x = requests.post(url, cookies=cookie, data=payload, verify=False)
if x.status_code == 200:
    print("success")
else:
    print(x.status_code, x.text)