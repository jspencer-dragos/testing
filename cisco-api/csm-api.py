#!/usr/bin/env python3

import requests
import urllib3
import ssl
import os
import xml.etree.ElementTree as ET

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

address = "10.20.8.60"
port = "443"
user_name = input("Username: ")
pass_word = input("Password: ")
firewalls = []

url_base = "https://" + address + ":" + port + "/nbi/"

# Get and store the certificate
cert = ssl.get_server_certificate((address, port))
cert_file = open('cert.crt', 'w')
cert_file.write(cert)
cert_file.close()

# Login
print("-----")
print("Login")
url = url_base + "login"
payload = f"""<?xml version="1.0" encoding="UTF-8"?>
<csm:loginRequest xmlns:csm="csm">
<protVersion>1.0</protVersion>
<reqId>123</reqId>
<username>{user_name}</username>
<password>{pass_word}</password>
<heartbeatRequested>true</heartbeatRequested>
<callbackUrl>{url}</callbackUrl>
</csm:loginRequest>"""
x = requests.post(url, data=payload, verify='cert.crt')
if x.status_code == 200:
    cookie = x.cookies.get_dict()
    print("success", cookie)
else:
    print(x.status_code, x.text)
    exit()

# List Devices
print("--------------")
print("List Devices")
url = url_base + "configservice/getDeviceListByType"
payload = """<?xml version="1.0" encoding="UTF-8"?>
<csm:deviceListByCapabilityRequest xmlns:csm="csm">
<protVersion>1.0</protVersion>
<reqId>123</reqId>
<deviceCapability>firewall</deviceCapability>
</csm:deviceListByCapabilityRequest>"""
x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
if x.status_code == 200:
    xml = ET.fromstring(x.text)
    for item in xml.findall('deviceId'):
        name = item.find('deviceName').text
        addr = item.find('ipv4Address').text
        firewalls.append(addr)
        print("Name:", name, "Address:", addr)
else:
    print(x.status_code, x.text)


# Get Dragos Network Policy Objects
def get_dragos_objects():
    print("--------------------------")
    print("Get Dragos Network Policy Objects")
    url = url_base + "configservice/getPolicyObjectsListByType"
    payload = """<?xml version="1.0" encoding="UTF-8"?>
    <csm:policyObjectsListByTypeRequest xmlns:csm="csm">
    <policyObjectType>NetworkPolicyObject</policyObjectType>
    </csm:policyObjectsListByTypeRequest>"""
    x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
    #print(x.status_code, x.text)
    if x.status_code == 200:
        xml = ET.fromstring(x.text)
        for obj in xml.findall('policyObject')[0]:
            name = obj.find('name').text
            if obj.find('isGroup').text == "false" and "dragos" in name:
                data = obj.find('ipData').text
                print ("Name:", name, "Address:", data)
    else:
        print(x.status_code, x.text)
    return
get_dragos_objects()

# Get Dragos Object Groups
def get_dragos_groups():
    print("--------------------------")
    print("Get Dragos Network Policy Groups")
    url = url_base + "configservice/getPolicyObjectsListByType"
    payload = """<?xml version="1.0" encoding="UTF-8"?>
    <csm:policyObjectsListByTypeRequest xmlns:csm="csm">
    <policyObjectType>NetworkPolicyObject</policyObjectType>
    </csm:policyObjectsListByTypeRequest>"""
    x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
    #print(x.status_code, x.text)
    if x.status_code == 200:
        xml = ET.fromstring(x.text)
        for obj in xml.findall('policyObject')[0]:
            grpname = obj.find('name').text
            if obj.find('isGroup').text == "true" and "dragos" in grpname:
                for item in obj.find('refGIDs'):
                    member = item.text
                    url = url_base + "configservice/getPolicyObjectByGID"
                    payload = f"""<?xml version="1.0" encoding="UTF-8"?><p:getPolicyObjectByGID xmlns:p="csm"><gid>{member}</gid></p:getPolicyObjectByGID>"""
                    x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
                    if x.status_code == 200:
                        xml2 = ET.fromstring(x.text)
                        for obj2 in xml2.find('policyObject'):
                            if obj2.find('isGroup').text == "false":
                                name = obj2.find('name').text
                                addr = obj2.find('ipData').text
                                print ("Group Name:", grpname, "Member:", name, "Address:", addr)
                    else:
                        print(x.status_code, x.text)
    else:
        print(x.status_code, x.text)
    return
get_dragos_groups()

a = input("Press ENTER to Continue....")

# Create a CSM Session
print("--------------------")
print("Create a CSM Session")
url = url_base + "configservice/createCSMSession"
payload = """<?xml version="1.0" encoding="UTF-8"?>
<p:newCSMSessionRequest xmlns:p="csm">
<csmSessionDescription>creating a Dragos CSM API session</csmSessionDescription>
</p:newCSMSessionRequest>"""
x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
if x.status_code == 200:
    xml = ET.fromstring(x.text)
    session_id = xml.find('csmSessionGID').text
    print("Session ID:", session_id)
else:
    print(x.status_code, x.text)

# Create a Network Policy Object
print("------------------------------")
print("Create a Network Policy Object")
url = url_base + "configservice/addPolicyObject"
payload = f"""<?xml version="1.0" encoding="UTF-8"?>
<csm:addPolicyObjectRequest xmlns:csm="csm">
<csmSessionGID>{session_id}</csmSessionGID>
<enforceDuplicateDetection>true</enforceDuplicateDetection>
<networkPolicyObject>
<name>dragos_obj2</name>
<type>NetworkPolicyObject</type>
<comment> </comment>
<isProperty>false</isProperty>
<subType>NH</subType>
<isGroup>false</isGroup>
<ipData>192.168.1.2</ipData>
</networkPolicyObject>
</csm:addPolicyObjectRequest>"""
x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
if x.status_code == 200:
    print("success")
else:
    print(x.status_code, x.text)

a = input("Press ENTER to Continue....")

# Validate CSM Session 
print("----------------")
print("Validate Changes")
url = url_base + "configservice/validateCSMSession"
payload = f"""<?xml version="1.0" encoding="UTF-8"?>
<ns1:csmSessionOperationRequest xmlns:ns1="csm">
<csmSessionGID>{session_id}</csmSessionGID>
</ns1:csmSessionOperationRequest>"""
x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
if x.status_code == 200:
    print("CSM Session changes validated")
    valid = True
else:
    print(x.status_code, x.text)
    valid = False

# Submit CSM Session
if valid == True:
    print("--------------")
    print("Submit changes")
    url = url_base + "configservice/submitCSMSession"
    payload = f"""<?xml version="1.0" encoding="UTF-8"?>
    <csm:submitCSMSessionRequest xmlns:csm="csm" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance ">
    <csmSessionGID>{session_id}</csmSessionGID>
    <submitComments>Dragos Submit</submitComments>
    <continueOnWarnings>true</continueOnWarnings>
    </csm:submitCSMSessionRequest>"""
    x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
    print(x.status_code, x.text)

get_dragos_groups()

# Logout
print("------")
print("Logout")
url = url_base + "logout"
payload = """<?xml version="1.0" encoding="UTF-8"?>
<csm:logoutRequest xmlns:csm="csm">
<protVersion>1.0</protVersion>
<reqId>123</reqId>
</csm:logoutRequest>"""
x = requests.post(url, cookies=cookie, data=payload, verify='cert.crt')
if x.status_code == 200:
    print("success")
else:
    print(x.status_code, x.text)

# Delete the cert file
os.remove("cert.crt")
