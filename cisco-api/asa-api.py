#!/usr/bin/env python3

## Test Cisco ASA API

import requests
import json
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

firewall = input("Firewall IP: ")
port = "443"
user_name = input("Username: ")
pass_word = input("Password: ")

url_base = "https://" + firewall + ":" + port

# Get a token
print("--------------------")
print("Get an API Token")
header = {'Content-Type': 'application/json', 'User-Agent': 'REST API Agent'}
url = url_base + "/api/tokenservices"
payload = {}
response = requests.post(url, auth=(user_name, pass_word), headers=header, data=json.dumps(payload), verify=False)
if response.status_code == 204:
    token = response.headers['X-Auth-Token']
    print("The token is:", token)
else:
    print("Something went wrong", response.status_code, response.text)
    exit()

header = {'Content-Type': 'application/json', 'User-Agent': 'REST API Agent', 'X-Auth-Token': token}

# List Address Objects
def list_objects():
    print("----------------------")
    print("List Address Objects")
    url = url_base + "/api/objects/networkobjects"
    response = requests.get(url, headers=header, verify=False)
    if response.status_code == 200:
        for object in response.json()['items']:
            if object['host']['kind'] == "IPv4Address":
                print(object['name'], object['host']['value'])
    else:
        print("Something went wrong", response.status_code)
    return
list_objects()

# Add an Address Object
print("---------------------")
print("Add an Address Object")
new_object_name = "dragos_obj2"
new_object_value = "192.168.1.2"
url = url_base + "/api/objects/networkobjects"
payload = {'kind':'object#NetworkObj', 'name':new_object_name, 'host':{'kind':'IPv4Address', 'value':new_object_value}, 'objectId':new_object_name}
response = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
if response.status_code == 201:
    print("Address object added successfully")
else:
    print("Something went wrong ", response.status_code)
list_objects()

# Rename an Address Object
print("------------------------")
print("Rename an Address Object")
new_object_name2 = "dragos_obj3"
url = url_base + "/api/objects/networkobjects/" + new_object_name
payload = {'name': new_object_name2}
response = requests.patch(url, headers=header, data=json.dumps(payload), verify=False)
if response.status_code == 204:
    print("Address Object Updated Successfully")
else:
    print("Something went wrong ", response.status_code)
list_objects()

# Change the IP of an Address Object
print("----------------------------------")
print("Change the IP of an Address Object")
new_object_value2 = "192.168.1.3"
url = url_base + "/api/objects/networkobjects/" + new_object_name2
payload = {'host':{'value': new_object_value2}}
response = requests.patch(url, headers=header, data=json.dumps(payload), verify=False)
if response.status_code == 204:
    print("Address Object Updated Successfully")
else:
    print("Something went wrong ", response.status_code)
list_objects()

# List Object Groups
def list_groups():
    print("--------------------------")
    print("List Network Object Groups")
    url = url_base + "/api/objects/networkobjectgroups"
    response = requests.get(url, headers=header, verify=False)
    if response.status_code == 200:
        for object in response.json()['items']:
            group_name = object['name']
            for member in object['members']:
                group_member = member['objectId']
                ref_link = member['refLink']
                response2 = requests.get(ref_link, headers=header, verify=False)
                if response2.status_code == 200:
                    ip_address = response2.json()['host']['value']
                    print(group_name, group_member, ip_address)
                else:
                    print(group_name, group_member)
    else:
        print("Something went wrong ", response.status_code)
    return
list_groups()

# Create an Address Group
print("-----------------------")
print("Create an Address Group")
url = url_base + "/api/objects/networkobjectgroups"
group_name = "dragos_group2"
payload = {'kind':'object#NetworkObjGroup', 'name':group_name, 'members':[{'kind':'objectRef#NetworkObj','objectId':'dragos_obj1'},{'kind':'objectRef#NetworkObj','objectId':new_object_name2}]}
response = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
if response.status_code == 201:
    print("The Address Group was created successfully")
else:
    print("Something went wrong", response.status_code)
list_groups()

# Update and Address Group
print("-----------------------")
print("Update an Address Group")
url = url_base + "/api/objects/networkobjectgroups/" + group_name
payload = {'members':[{'kind':'objectRef#NetworkObj','objectId':'dragos_obj1'}]}
response = requests.patch(url, headers=header, data=json.dumps(payload), verify=False)
if response.status_code == 204:
    print("The Address Group was updated successfully")
else:
    print("Something went wrong", response.status_code)
list_groups()

# Delete an Address Group
print("-----------------------")
print("Delete an Address Group")
url = url_base + "/api/objects/networkobjectgroups/" + group_name
response = requests.delete(url, headers=header, verify=False)
if response.status_code == 204:
    print("The Address Group was Deleted Successfully")
else:
    print("Something went wrong ", response.status_code)
    print(response.text)
list_groups()

# Delete an Address Object
print("------------------------")
print("Delete an Address Object")
url = url_base + "/api/objects/networkobjects/" + new_object_name2
response = requests.delete(url, headers=header, verify=False)
if response.status_code == 204:
    print("The Address Object was Deleted Successfully")
else:
    print("Something went wrong ", response.status_code)
list_objects()

# Commit Changes
print("--------------")
print("Commit Changes")
url = url_base + "/api/commands/writemem"
payload = {}
response = requests.post(url, headers=header, verify=False)
print(response.status_code, response.text)

# Delete the token
print("-----------------------")
print("Delete the API Token")
url = url_base + "/api/tokenservices/" + token
response = requests.delete(url, headers=header, verify=False)
if response.status_code == 204:
    print("The token was deleted successfully")
else:
    print("The token was NOT deleted")
