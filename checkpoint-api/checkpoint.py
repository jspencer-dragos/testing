#!/usr/bin/env python

## Works on CheckPoint R80.40, R81.00, R81.10

import requests
import json
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

firewall = input("Firewall IP: ")
port = "443"
user_name = input("Username: ")
pass_word = input("Password: ")

url_base = "https://" + firewall + ":" + port + "/web_api/"

## Login
print("------------")
print("Login")
url = url_base + "login"
header = {'Content-Type' : 'application/json'}
payload = {'user': user_name, 'password': pass_word}
login = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
if login.status_code == 200:
    sid = login.json()['sid']
    header = {'Content-Type' : 'application/json', 'X-chkp-sid' : sid}
    print(login.text)
else:
    print(login.status_code, login.text)
    exit()

## List Hosts
def list_hosts():
    print("---------------")
    print("List Hosts")
    url = url_base + "show-hosts"
    payload = {}
    show = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
    for object in show.json()['objects']:
        print(object['name'], object['ipv4-address'])
    return
list_hosts()

## Add Host
print("-----------")
print("Add Host")
url = url_base + "add-host"
payload = {'name':'dragos_test_1', 'ip-address':'192.168.1.1'}
add = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(add)

## Publish
def publish():
    print("----------------------")
    print("Publishing Changes")
    url = url_base + "publish"
    payload = {}
    commit = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
    print(commit)
    return
publish()
list_hosts()
input("Wait.....")

## Rename Host
print("------------")
print("Rename Host")
url = url_base + "set-host"
payload = {'name':'dragos_test_1', 'new-name':'dragos_test_2'}
rename = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(rename)
publish()
list_hosts()
input("Wait.....")

## Change IP
print("--------------")
print("Change IP")
url = url_base + "set-host"
payload = {'name':'dragos_test_2', 'ip-address':'192.168.1.2'}
update = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(update)
publish()
list_hosts()
input("Wait.....")

## List Groups
def list_groups():
    print("------------------")
    print("List Groups")
    url = url_base + "show-groups"
    payload = {}
    groups = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
    for item in groups.json()['objects']:
        group_name = item['name']
        url = url_base + "show-group"
        payload = {'name':group_name}
        group = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
        for member in group.json()['members']:
            host_name = member['name']
            url = url_base + "show-host"
            payload = {'name':host_name}
            host = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
            if 'ipv4-address' in host.json():
                ip_address = host.json()['ipv4-address']
            elif 'ip-address' in host.json():
                ip_address = host.json()['ip-address']
            else:
                ip_address = ''
            #print(host, host.json())
            print(group_name, host_name, ip_address)
    return
list_groups()

## Add Group
print("-------------------")
print("Add Group")
url = url_base + "add-group"
payload = {'name':'dragos_test_group2', 'members':'dragos_test_2'}
add_group = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(add_group)
publish()
list_groups()
input("Wait.....")

## Rename Group
print("-------------")
print("Rename Group")
url = url_base + "set-group"
payload = {'name':'dragos_test_group2', 'new-name':'dragos_test_group3'}
rename_group = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(rename_group)
publish()
list_groups()
input("Wait.....")

## Change Group
print("--------------")
print("Change Group")
url = url_base + "set-group"
payload = {'name':'dragos_test_group3', 'members':['dragos_test_2','dragos_test_3']}
change_group = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(change_group)
publish()
list_groups()
input("Wait.....")

## Delete Group
print("--------------")
print("Delete Group")
url = url_base + "delete-group"
payload = {'name':'dragos_test_group3'}
delete_group = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(delete_group)
publish()
list_groups()
input("Wait.....")

## Delete Host
print("--------------")
print("Delete Host")
url = url_base + "delete-host"
payload = {'name':'dragos_test_2'}
delete = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print(delete)
publish()
list_hosts()
input("Wait.....")

## Logout
print("------------")
print("Logout")
url = url_base + "logout"
payload = {}
logout = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print("Logout result: " + logout.json()['message'])
