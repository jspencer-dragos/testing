#!/usr/bin/env python3

import requests
import json
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

firewall= "10.20.8.57"
port = "443"
user_name = ""
pass_word = ""
api_key = "RQ9v2NWt91ZI8XsymAVseQ=="

url_base = "https://" + firewall + ":" + port + "/web_api/"

# Login
print("------------")
print("Login")
url = url_base + "login"
header = {'Content-Type' : 'application/json'}
payload = {'api-key': api_key}
#payload = {'user': user_name, 'password': pass_word}
print(payload)
login = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
if login.status_code == 200:
    sid = login.json()['sid']
    header = {'Content-Type' : 'application/json', 'X-chkp-sid' : sid}
    print(login.text)
else:
    print(login.status_code, login.text)
    exit()


## Logout
print("------------")
print("Logout")
url = url_base + "logout"
payload = {}
logout = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print("Logout result: " + logout.json()['message'])