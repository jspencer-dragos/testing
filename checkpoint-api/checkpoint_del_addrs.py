#!/usr/bin/env python3

## Delete Dragos address objects from CheckPoint

import getpass
import requests
import json
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

firewall = input("Firewall IP: ") or '10.20.8.57'
port = "443"
user_name = input("Username: ") or 'admin'
pass_word = getpass.getpass()


url_base = "https://" + firewall + ":" + port + "/web_api/"

## Login
print("------------")
print("Login")
url = url_base + "login"
header = {'Content-Type' : 'application/json'}
payload = {'user': user_name, 'password': pass_word}
login = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
if login.status_code == 200:
    sid = login.json()['sid']
    header = {'Content-Type' : 'application/json', 'X-chkp-sid' : sid}
    print(login.text)
else:
    print(login.status_code, login.text)
    exit()

## Publish
def publish():
    print("----------------------")
    print("Publishing Changes")
    url = url_base + "publish"
    payload = {}
    commit = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
    print(commit)
    return

# Delete Groups
print("------------------")
url = url_base + "show-groups"
payload = {}
groups = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
for item in groups.json()['objects']:
    group_name = item['name']
    url = url_base + "delete-group"
    payload = {'name':group_name}
    delete_group = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
    if delete_group.status_code == 200:
        print("Deleted group: " + group_name)
    else:
        print("Failed to delete group: " + group_name + ":" + str(delete_group.status_code))
publish()


## Delete Hosts
count = 1
page = 1
while count > 0:
    print("---------------")
    print("Page number: " + str(page))
    page +=1
    url = url_base + "show-hosts"
    payload = {}
    show = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
    count = 0
    for object in show.json()['objects']:
        if (object['name']).startswith("Dragos"):
            count +=1
            print(object['name'])
            url = url_base + "delete-host"
            payload = {'name':object['name']}
            delete = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
            if delete.status_code == 200:
                print("Deleted host object: " + object['name'])
            else:
                print("Failed to delete host object: " + object['name'])
                print(delete.text)
    publish()


## Logout
print("------------")
print("Logout")
url = url_base + "logout"
payload = {}
logout = requests.post(url, headers=header, data=json.dumps(payload), verify=False)
print("Logout result: " + logout.json()['message'])