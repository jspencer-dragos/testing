#!/usr/bin/env python3

import time
import requests
import json
import xml.etree.ElementTree as ET
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

firewall = input("Firewall IP: ")
port = "443"
user_name = input("Username: ")
pass_word = input("Password: ")
api_key = input("API key: ")

url_base = "https://" + firewall + ":" + port

def req_api_key():
    # Request an API key
    print("------------------")
    print("Request an API Key")
    url = url_base + "/api?type=keygen&user=" + user_name + "&password=" + pass_word
    response = requests.get(url, verify=False)
    xml = ET.fromstring(response.text)
    if response.status_code == 200 and xml.attrib['status'] == "success":
        api_key = xml[0][0].text
        print("API KEY:", api_key)
    else:
        print("Something went wrong", response.status_code)
    return

# Generate the API Key if it doesn't exist.
if api_key == "":
    req_api_key()
else:
    print("API KEY already exists")

# Add the API key to the header
req_header = { "X-PAN-KEY": api_key }

# List Address Objects
def list_addr_obj():
    print("-----------------------")
    print("Listing Address Objects")
    req_url = url_base + "/restapi/v10.1/Objects/Addresses?location=vsys&vsys=vsys1"
    this_req = requests.get(req_url, headers= req_header, verify=False)
    if this_req.status_code == 200:
        output = json.loads(this_req.text)
        for item in output["result"]["entry"]:
            print(item["@name"], item["ip-netmask"])
    else:
        print("Something went wrong", this_req.status_code, this_req.text)
    return
list_addr_obj()

# Create Address Object
print("---------------------")
print("Adding Address Object")
addr_name = "dragos_address5"
addr_ip = "192.168.2.5"
addr_obj = {"entry": {"@name": addr_name, "ip-netmask": addr_ip}}
addr_obj_json = json.dumps(addr_obj)
post_addr_url = url_base + "/restapi/v10.1/Objects/Addresses?location=vsys&vsys=vsys1&name=" + addr_name
post_addr = requests.post(post_addr_url, headers=req_header, data=addr_obj_json, verify=False)
if post_addr.status_code == 200:
    addr_resp = json.loads(post_addr.text)
else:
    print("Something went wrong", post_addr.status_code, post_addr.text)
list_addr_obj()

#Rename Address Object
print("---------------------")
print("Rename Address Object")
addr_new_name = "dragos_address6"
ren_addr_url = url_base + "/restapi/v10.1/Objects/Addresses:rename?location=vsys&vsys=vsys1&name=" + addr_name + "&newname=" + addr_new_name
ren_addr = requests.post(ren_addr_url, headers=req_header, verify=False)
if ren_addr.status_code == 200:
    ren_resp = json.loads(ren_addr.text)
else:
    print("Something went wrong", ren_addr.status_code, ren_addr.text)
list_addr_obj()

#Update Address Object
print("---------------------")
print("Update Address Object")
addr_new_ip = "192.168.2.6"
update_addr_data = {"entry": {"@name": addr_new_name, "ip-netmask": addr_new_ip}}
update_addr_data_json = json.dumps(update_addr_data)
update_addr_url = url_base + "/restapi/v10.1/Objects/Addresses?location=vsys&vsys=vsys1&name=" + addr_new_name
update_addr = requests.put(update_addr_url, headers=req_header, data=update_addr_data_json, verify=False)
if update_addr.status_code == 200:
    update_resp = json.loads(update_addr.text)
else:
    print("Something went wrong", update_addr.status_code, update_addr.text)
list_addr_obj()

# List Address Groups
def list_address_groups():
    print("----------------------")
    print("Listing Address Groups")
    grp_list_url = url_base + "/restapi/v10.1/Objects/AddressGroups?location=vsys&vsys=vsys1"
    list_req = requests.get(grp_list_url, headers=req_header, verify=False)
    if list_req.status_code == 200:
        list_resp = json.loads(list_req.text)
        for item in list_resp["result"]["entry"]:
            list_name = item["@name"]
            for member in item["static"]["member"]:
                print(list_name, member)
    else:
        print("Something went wrong", list_req.status_code, list_req.text)
    return
list_address_groups()

# Create an Address Group
print("-------------------------")
print("Creating an Address Group")
addr_grp_name = "dragos_group2"
addr_list = ['dragos_address_1', 'dragos_address_3']
addr_grp_data = {"entry": {"@name": addr_grp_name, "static": {"member": addr_list}}}
addr_grp_data_json = json.dumps(addr_grp_data)
#print(addr_grp_data_json)
addr_grp_url = url_base + "/restapi/v10.1/Objects/AddressGroups?location=vsys&vsys=vsys1&name=" + addr_grp_name
addr_grp_post = requests.post(addr_grp_url, headers=req_header, data=addr_grp_data_json, verify=False)
if addr_grp_post.status_code == 200:
    addr_grp_resp = json.loads(addr_grp_post.text)
else:
    print("Something went wrong", addr_grp_post.status_code, addr_grp_post.text)
list_address_groups()

#Edit Address Group
print("------------------")
print("Edit Address Group")
edit_grp_data = {"entry": {"@name": addr_grp_name, "static": {"member": ['dragos_address_1']}}}
edit_grp_data_json = json.dumps(edit_grp_data)
edit_grp_url = url_base + "/restapi/v10.1/Objects/AddressGroups?location=vsys&vsys=vsys1&name=" + addr_grp_name
edit_grp_put = requests.put(edit_grp_url, headers=req_header, data=edit_grp_data_json, verify=False)
if edit_grp_put.status_code == 200:
    print("Updated Address Group:", addr_grp_name)
    edit_grp_resp = json.loads(edit_grp_put.text)
else:
    print("Something went wrong", edit_grp_put.status_code, edit_grp_put.text)
list_address_groups()

# Rename an Address Group
print("-------------------------")
print("Renaming an Address Group")
addr_grp_new_name = "dragos_group3"
ren_grp_url = url_base + "/restapi/v10.1/Objects/AddressGroups:rename?location=vsys&vsys=vsys1&name=" + addr_grp_name + "&newname=" + addr_grp_new_name
ren_grp_post = requests.post(ren_grp_url, headers=req_header, verify=False)
if ren_grp_post.status_code == 200:
    print("Renamed Address Group:", addr_grp_name, "to", addr_grp_new_name)
    ren_grp_resp = json.loads(ren_grp_post.text)
else:
    print("Something went wrong", ren_grp_post.status_code, ren_grp_post.text)
list_address_groups()

# Delete Address Group
print("--------------------")
print("Delete Address Group")
del_grp_url = url_base + "/restapi/v10.1/Objects/AddressGroups?location=vsys&vsys=vsys1&name=" + addr_grp_new_name
del_grp_del = requests.delete(del_grp_url, headers=req_header, verify=False)
if del_grp_del.status_code == 200:
    del_grp_resp = json.loads(del_grp_del.text)
    print("Deleted Address Group:", addr_grp_name)
else:
    print("Something went wrong", del_grp_del.status_code, del_grp_del.text)
list_address_groups()

# Delete Address Object
print("---------------------")
print("Delete Address Object")
del_addr_url = url_base + "/restapi/v10.1/Objects/Addresses?location=vsys&vsys=vsys1&name=" + addr_new_name
del_addr = requests.delete(del_addr_url, headers=req_header, verify=False)
if del_addr.status_code == 200:
    del_resp = json.loads(del_addr.text)
else:
    print("Something went wrong", del_addr.status_code, del_addr.text)
list_addr_obj()


# Commit the Changes
def commit():
    print("-------------------------")
    print("Check for Pending Changes")
    url = url_base + "/api/?type=op&cmd=<check><pending-changes></pending-changes></check>&key=" + api_key
    response = requests.get(url, verify=False)
    xml = ET.fromstring(response.text)
    if response.status_code == 200 and xml.attrib['status'] == "success":
        if xml[0].text == "yes":
            print("------------------")
            print("Commit the Changes")
            url = url_base + "/api/?key=" + api_key + "&type=commit&cmd=<commit></commit>"
            response = requests.get(url, verify=False)
            xml = ET.fromstring(response.text)
            if response.status_code == 200 and xml.attrib['status'] == "success":
                job = xml[0][1].text
                status = "ACT"
                url = url_base + "/api/?type=op&cmd=<show><jobs><id>" + job + "</id></jobs></show>&key=" + api_key
                while status == "ACT":
                    response = requests.get(url, verify=False)
                    xml = ET.fromstring(response.text)
                    status = xml[0][0][5].text
                    print("Waiting on job", job, "to complete. ", status)
                    time.sleep(5)
            else:
                print("Something went wrong", response.status_code)
        else:
            print("No Changes to Commit")
    else:
        print("Something went wrong", response.status_code, response.text)
    return
#commit()
