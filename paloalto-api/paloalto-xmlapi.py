#!/usr/bin/env python3

import time
import requests
import json
import xml.etree.ElementTree as ET
from requests import api
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

firewall = input("Firewall IP: ")
port = "443"
user_name = input("Username: ")

pass_word = input("Password: ")

api_key = input("API key: ")

url_base = "https://" + firewall + ":" + port

def req_api_key():
    # Request an API key
    print("------------------")
    print("Request an API Key")
    url = url_base + "/api?type=keygen&user=" + user_name + "&password=" + pass_word
    response = requests.get(url, verify=False)
    xml = ET.fromstring(response.text)
    if response.status_code == 200 and xml.attrib['status'] == "success":
        api_key = xml[0][0].text
        print("API KEY:", api_key)
    else:
        print("Something went wrong", response.status_code)
    return

if api_key == "":
    req_api_key()
else:
    print("API KEY already exists")

#header = {"X-PAN-KEY": api_key}

# List Address Objects
def list_addresses():
    print("-----------------------")
    print("List Address Objects")
    url = url_base + "/api/?type=config&action=get&xpath=/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/address&key=" + api_key
    response = requests.get(url, verify=False)
    xml = ET.fromstring(response.text)
    if response.status_code == 200 and xml.attrib['status'] == "success":
        for entry in xml[0][0]:
            name = entry.attrib['name']
            value = entry[0].text
            print(name, value)
    else:
        print("Something went wrong", response.status_code)
    return
list_addresses()

# List Address Groups
def list_groups():
    print("--------------------------")
    print("List Address Group Objects")
    url = url_base + "/api/?type=config&action=get&xpath=/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/address-group&key=" + api_key
    response = requests.get(url, verify=False)
    xml = ET.fromstring(response.text)
    if response.status_code == 200 and xml.attrib['status'] == "success":
        #print(response.text)
        for entry in xml[0][0]:
            group_name = entry.attrib['name']
            for member in entry[0]:
                member_name = member.text
                print(group_name, member_name)
    else:
        print("Something went wrong", response.status_code)
    return
list_groups()

# Create an Address Object
print("---------------------")
print("Create Address Object")
name = "dragos_address5"
ip = "192.168.1.5/32"
xpath = "/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/address/entry[@name='" + name + "']"
url = url_base + "/api/?type=config&action=set&key=" + api_key + "&xpath=" + xpath + "&element=<ip-netmask>" + ip + "</ip-netmask>"
response = requests.get(url, verify=False)
xml = ET.fromstring(response.text)
if response.status_code == 200 and xml.attrib['status'] == "success":
    print("Created Address Object: ", name)
else:
    print("Something went wrong", response.status_code)
list_addresses()

# Create an Address Group
print("-----------------------")
print("Create an Address Group")
group_name = "dragos_group3"
group_members = "<member>dragos_address5</member>"
xpath = "/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/address-group/entry[@name='" + group_name + "']/static"
url = url_base + "/api/?type=config&action=set&key=" + api_key + "&xpath=" + xpath + "&element=" + group_members
response = requests.get(url, verify=False)
xml = ET.fromstring(response.text)
if response.status_code == 200 and xml.attrib['status'] == "success":
    print("Created Address Group: ", group_name)
else:
    print("Something went wrong", response.status_code, xml.attrib['status'])
    print(response.text)
list_groups()

# Modify an Address Group
print("-----------------------")
print("Modify an Address Group")
group_name = "dragos_group3"
# Static required in the member list for the edit action
group_members = "<static><member>dragos_address5</member></static>"
xpath = "/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/address-group/entry[@name='" + group_name + "']/static"
url = url_base + "/api/?type=config&action=edit&key=" + api_key + "&xpath=" + xpath + "&element=" + group_members
response = requests.get(url, verify=False)
xml = ET.fromstring(response.text)
if response.status_code == 200 and xml.attrib['status'] == "success":
    print("Modified Address Group: ", group_name)
else:
    print("Something went wrong", response.status_code, xml.attrib['status'])
    print(response.text)
list_groups()

# Delete an Address Group
print("-----------------------")
print("Delete an Address Group")
xpath = "/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/address-group/entry[@name='" + group_name + "']"
url = url_base + "/api/?key=" + api_key + "&type=config&action=delete&xpath=" + xpath
response = requests.get(url, verify=False)
xml = ET.fromstring(response.text)
if response.status_code == 200 and xml.attrib['status'] == "success" and xml.attrib['code'] == "20":
    print("Deleted Address Group:", name)
else:
    print("Something went wrong", response.status_code, response.text)

# Delete an Address Object
print("---------------------- -")
print("Delete an Address Object")
xpath = "/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/address/entry[@name='" + name + "']"
url = url_base + "/api/?key=" + api_key + "&type=config&action=delete&xpath=" + xpath
response = requests.get(url, verify=False)
xml = ET.fromstring(response.text)
if response.status_code == 200 and xml.attrib['status'] == "success" and xml.attrib['code'] == "20":
    print("Deleted Address:", name)
else:
    print("Something went wrong", response.status_code, response.text)
list_addresses()

# Commit the Changes
def commit():
    print("-------------------------")
    print("Check for Pending Changes")
    url = url_base + "/api/?type=op&cmd=<check><pending-changes></pending-changes></check>&key=" + api_key
    response = requests.get(url, verify=False)
    xml = ET.fromstring(response.text)
    if response.status_code == 200 and xml.attrib['status'] == "success":
        if xml[0].text == "yes":
            print("------------------")
            print("Commit the Changes")
            url = url_base + "/api/?key=" + api_key + "&type=commit&cmd=<commit></commit>"
            response = requests.get(url, verify=False)
            xml = ET.fromstring(response.text)
            if response.status_code == 200 and xml.attrib['status'] == "success":
                job = xml[0][1].text
                status = "ACT"
                url = url_base + "/api/?type=op&cmd=<show><jobs><id>" + job + "</id></jobs></show>&key=" + api_key
                while status == "ACT":
                    response = requests.get(url, verify=False)
                    xml = ET.fromstring(response.text)
                    status = xml[0][0][5].text
                    print("Waiting on job", job, "to complete. ", status)
                    time.sleep(5)
            else:
                print("Something went wrong", response.status_code)
        else:
            print("No Changes to Commit")
    else:
        print("Something went wrong", response.status_code, response.text)
    return
commit()
