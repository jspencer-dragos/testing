#!/bin/bash
## Free up shards

threshold=99

# Elastic container address
address=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' elasticsearch)

# Total available per node
total=$(curl "http://$address:9200/_cluster/settings?include_defaults=true&filter_path=defaults.cluster.max_shards_per_node" 2>/dev/null | awk -F: '{print $NF}' | sed -e 's/[\"|}]//g' | xargs)
echo "Total Shards: $total"

function shards {
  # Total shards used
  used=$(curl http://$address:9200/_cluster/stats?filter_path=indices.shards.total 2>/dev/null | awk -F: '{print $NF}' | sed -e 's/[\"|}]//g' | xargs)
  echo "Shards Used: $used"

  # Percent used
  percent=$(echo | awk -v a="$used" -v b="$total" '{printf "%.0f\n", (a/b)*100 }')
}
shards

while [ "$percent" -ge "$threshold" ]
do
  oldest=$(curl http://$address:9200/_cat/indices 2>/dev/null | grep pipeline | awk '{print $3}' | awk -F_ '{print $NF}' | sort | head -1)
  pipeline="pipeline_$oldest"
  echo "Deleting index $pipeline"
  delete=$(curl -X DELETE http://$address:9200/$pipeline 2>/dev/null)
  shards
done
