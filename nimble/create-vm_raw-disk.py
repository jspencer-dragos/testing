#!/usr/bin/env python3

## Create a VM with no disk

import sys
import argparse
import requests
import urllib3
from pyVmomi import vim
from pyVim.connect import SmartConnect, Disconnect
from pyVim.task import WaitForTask

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
#urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)



# Variables
vcenter = "eng-vcenter.hq.dragos.services"
datacenter_name = "Colo-Engineering"
cluster_name = "EngOps-Colo"
esxi_host_name = "eng-hv302.bwi.dragos.services"
folder_name = "LREs"
datastore_name = "EngOps_Colo"
portgroup_name = "VLAN 2114 (QE AUTO)"
hostname = "zcc-test"
comment = "Zero-Copy-Clone Test"
ram_mb = 96000
num_cpu = 24
guest_os = "debian11_64Guest"


parser = argparse.ArgumentParser()
parser.add_argument("--vcenter_username", help='vCenter Username', required=True)
parser.add_argument("--vcenter_password", help='vCenter Password', required=True)
parser.add_argument("--serial", help='SAN LUN Serial NUmber', required=True)
args = parser.parse_args()

# Function to lookup IDs from Names
def helper(connection, vim_type, name, folder=None, recurse=True):
    """
    Search objects for the name and type specified.
    Sample:
    helper(c, [vim.Datastore], "HQ-Engineering")
    """
    if folder is None:
        folder = connection.content.rootFolder
    obj = None
    container = connection.content.viewManager.CreateContainerView(folder, vim_type, recurse)

    for object_ref in container.view:
        if object_ref.name == name:
            obj = object_ref
            break
    container.Destroy()
    return obj

# Function to disconnect
def disconnect():
    try:
        Disconnect(c)
        return "Disconnected"
    except:
        return "Failed to Disconnect"

# Login to vCenter
try:
    c = SmartConnect(host=vcenter, user=args.vcenter_username, pwd=args.vcenter_password, disableSslCertValidation=True)
except:
    print("Connection failed")
    sys.exit()

# Convert names to IDs
datacenter = helper(c, [vim.Datacenter], datacenter_name)
folder = helper(c, [vim.Folder], folder_name)
datastore = helper(c, [vim.Datastore], datastore_name)
cluster = helper(c, [vim.ClusterComputeResource], cluster_name)
esxi_host = helper(c, [vim.HostSystem], esxi_host_name)
network = helper(c, [vim.Network], portgroup_name)
resource_pool = esxi_host.parent.resourcePool
#print(datacenter, folder, datastore, cluster, esxi_host, network, resource_pool)

# Rescan All HBAs
esxi_host.configManager.storageSystem.RescanAllHba()

# Refresh Storage
refresh_storage = esxi_host.configManager.storageSystem.RefreshStorageSystem()

# VM File Info (Required by Config Spec)
fileinfo = vim.vm.FileInfo()
fileinfo.vmPathName = "["+datastore_name+"]"

# NIC Device Spec (Not Required but needed.)
# https://vdc-download.vmware.com/vmwb-repository/dcr-public/3325c370-b58c-4799-99ff-58ae3baac1bd/45789cc5-aba1-48bc-a320-5e35142b50af/doc/vim.vm.device.VirtualDeviceSpec.html
# https://vdc-download.vmware.com/vmwb-repository/dcr-public/3325c370-b58c-4799-99ff-58ae3baac1bd/45789cc5-aba1-48bc-a320-5e35142b50af/doc/vim.vm.device.VirtualEthernetCard.html
nicspec = vim.vm.device.VirtualDeviceSpec()
nicspec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
nicspec.device = vim.vm.device.VirtualVmxnet3()
nicspec.device.addressType = 'Generated'
#nicspec.device.addressType = 'Manual'
#nicspec.device.macAddress = '00:00:00:00:00:00'
nicspec.device.backing = vim.vm.device.VirtualEthernetCard.NetworkBackingInfo()
nicspec.device.backing.useAutoDetect = False
nicspec.device.backing.deviceName = portgroup_name
nicspec.device.deviceInfo = vim.Description()
nicspec.device.deviceInfo.summary = "Network Card"
nicspec.device.connectable = vim.vm.device.VirtualDevice.ConnectInfo()
nicspec.device.connectable.startConnected = True
nicspec.device.connectable.connected = True

# SCSI Controller Spec (Required by Disk Spec.)
# https://demitasse.co.nz/2018/06/adding-scsi-controllers-and-disks-with-pyvmomi/
scsispec = vim.vm.device.VirtualDeviceSpec()
scsispec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
scsispec.device = vim.vm.device.ParaVirtualSCSIController()
scsispec.device.sharedBus = "noSharing"
scsispec.device.busNumber = 0
scsispec.device.hotAddRemove = True

# Disk Device Spec (Not Required but needed.)
# https://vdc-repo.vmware.com/vmwb-repository/dcr-public/723e7f8b-4f21-448b-a830-5f22fd931b01/5a8257bd-7f41-4423-9a73-03307535bd42/doc/vim.vm.device.VirtualDisk.RawDiskMappingVer1BackingInfo.html
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/add_raw_disk_to_vm.py
diskspec = vim.vm.device.VirtualDeviceSpec()
diskspec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
diskspec.fileOperation = 'create'
diskspec.device = vim.vm.device.VirtualDisk()
diskspec.device.backing = vim.vm.device.VirtualDisk.RawDiskMappingVer1BackingInfo()
diskspec.device.backing.compatibilityMode = 'physicalMode'
diskspec.device.backing.diskMode = 'independent_persistent'
diskspec.device.unitNumber = 0
lun_path = '/vmfs/devices/disks/eui.'+args.serial
diskspec.device.backing.deviceName = lun_path
diskspec.device.controllerKey = scsispec.device.key

# Boot Options (Used by Config Spec. Not Required)
# https://vdc-download.vmware.com/vmwb-repository/dcr-public/3325c370-b58c-4799-99ff-58ae3baac1bd/45789cc5-aba1-48bc-a320-5e35142b50af/doc/vim.vm.BootOptions.html
bootopts = vim.vm.BootOptions()
bootopts.bootDelay = 1000
bootopts.bootRetryEnabled = False
bootopts.efiSecureBootEnabled = False
bootopts.enterBIOSSetup = False
bootdisk = vim.vm.BootOptions.BootableDiskDevice()
bootdisk.deviceKey = diskspec.device.key
bootopts.bootOrder = [bootdisk]

# Configuration Spec (Required)
# https://vdc-download.vmware.com/vmwb-repository/dcr-public/3325c370-b58c-4799-99ff-58ae3baac1bd/45789cc5-aba1-48bc-a320-5e35142b50af/doc/vim.vm.ConfigSpec.html
configspec = vim.vm.ConfigSpec()
configspec.name = hostname.upper()
configspec.annotation = comment
configspec.bootOptions = bootopts
configspec.firmware = "efi"
configspec.memoryMB = ram_mb
configspec.numCPUs = num_cpu
configspec.files = fileinfo
configspec.guestId = guest_os
configspec.deviceChange = [ nicspec, scsispec, diskspec ]

# Create the VM
try:
    task = folder.CreateVm(configspec, pool=resource_pool, host=esxi_host)
    WaitForTask(task)
except vim.fault.DuplicateName:
    print("VM already exists")
    dis = disconnect()
    sys.exit()
except vim.fault.AlreadyExists:
    print("VM already exists")
    dis = disconnect()
    sys.exit()
except:
    print("Failed")
    dis = disconnect()
    print(dis)
    sys.exit()    

# Find the VM that was just created
try:
    vm = helper(c, [vim.VirtualMachine], hostname.upper())
    if vm is None:
        raise ValueError()
    print(vm)
except:
    print("Unable to find VM")
    dis = disconnect()
    print(dis)
    sys.exit()

# Upload NVRAM file
# https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/upload_file_to_datastore.py
params = {'dsName': datastore.info.name, 'dcPath': datacenter.name}
print(params)
resource = "/folder/"+hostname.upper()+"/"+hostname.upper()+".nvram"
url = "https://" + vcenter + ":443" + resource
print(url)
client_cookie = c._stub.cookie
cookie_name = client_cookie.split("=", 1)[0]
cookie_value = client_cookie.split("=", 1)[1].split(";", 1)[0]
cookie_path = client_cookie.split("=", 1)[1].split(";", 1)[1].split(
    ";", 1)[0].lstrip()
cookie_text = " " + cookie_value + "; $" + cookie_path
cookie = dict()
cookie[cookie_name] = cookie_text
headers = {'Content-Type': 'application/octet-stream'}
with open('./ZCC.nvram', 'rb') as nvram_file:
    upload = requests.put(url, params=params, headers=headers, cookies=cookie, data=nvram_file, verify=False)
    #print(upload.status_code, upload.text)
if upload.status_code != 201:
    print("Upload failed")
    print(upload.text)
    disconnect()
    sys.exit()

# Power on the VM
try:
    power_on = vm.PowerOn()
    WaitForTask(power_on)
    print(power_on.info)
except:
    print("Power On Failed")
    disconnect()
    sys.exit()

## Disconnect
dis = disconnect()
print(dis)