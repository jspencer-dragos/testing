#!/usr/bin/env python3

## https://infosight.hpe.com/InfoSight/media/cms/active/public/pubs_REST_API_Reference_NOS_51x.whz/jun1455055569904.html

import sys
import requests
import urllib3
import argparse
import json

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nimble Username", required=True)
parser.add_argument("--password", help="Nimble Password", required=True)
args = parser.parse_args()

base_url = "https://10.21.0.20:5392/v1/"

# Get a user token
url = base_url + "tokens"
data = {'data': {'username': args.username, 'password': args.password}}
x = requests.post(url, json=data, verify=False)

if x.status_code == 201:
    id = x.json()['data']['id']
    token = x.json()['data']['session_token']
    print(token)
    header = {'X-Auth-Token': token }
else:
    print("oops")
    sys.exit()

# List LUNs
url = base_url + "volumes/detail"
x = requests.get(url, headers=header, verify=False)
#print(x.status_code, x.text)
if x.status_code == 200:
    for item in x.json()['data']:
        print(item['name'])
        print(item['serial_number'])
        if item['clone'] == True:
            print(item['id'])
            print(item['target_name'])
            print(item['clone'])
            print(item['vpd_ieee0'])
            print(item['vpd_ieee1'])
            print(item['vpd_t10'])
            print(item)
        print("---------------------------------------------------------------")
else:
    print("Something went wrong")

# Delete token
url = base_url + "tokens/" + id
header = {'X-Auth-Token': token }
x = requests.delete(url, headers=header, verify=False)