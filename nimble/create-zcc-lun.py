#!/usr/bin/env python3

## Create a zero copy clone

## https://infosight.hpe.com/InfoSight/media/cms/active/public/pubs_REST_API_Reference_NOS_51x.whz/jun1455055569904.html

import sys
import requests
import urllib3
import argparse
import json

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parser = argparse.ArgumentParser()
parser.add_argument("--username", help="Nimble Username", required=True)
parser.add_argument("--password", help="Nimble Password", required=True)
args = parser.parse_args()


### Nimble SAN Section

base_url = "https://10.21.0.20:5392/v1/"
volume = "STS-200-VM"
clone_name = "STS-200-Clone1"

# Get a user token
url = base_url + "tokens"
data = {'data': {'username': args.username, 'password': args.password}}
x = requests.post(url, json=data, verify=False)

if x.status_code == 201:
    id = x.json()['data']['id']
    token = x.json()['data']['session_token']
    print(token)
else:
    print("oops")
    sys.exit()

# Find the snapshot to clone
url = base_url + "snapshots?vol_name=" + volume
header = {'X-Auth-Token': token }
x = requests.get(url, headers=header, verify=False)
if x.status_code == 200:
    latest_snap = (sorted(x.json()['data'], key=lambda d: d['name'], reverse=True))[0]
    latest_snap_id = latest_snap['id']
    print(latest_snap_id)
else:
    print('oops')
    sys.exit()

# Create the clone
url = base_url + "volumes"
header = {'X-Auth-Token': token }
data = {'data':{'name': clone_name, 'clone': True, 'base_snap_id': latest_snap_id}}
x = requests.post(url, headers=header, json=data, verify=False)
if x.status_code == 201:
    print("Clone created")
    clone_id = x.json()['data']['serial_number']
    print(clone_id)
else:
    print("Clone failed")

# Delete token
url = base_url + "tokens/" + id
header = {'X-Auth-Token': token }
x = requests.delete(url, headers=header, verify=False)
