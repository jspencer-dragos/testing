#!/usr/bin/env python3

## Get a list of known repositories from the Bitbucket API

## Generate an app password for the Bitbucket API here: https://bitbucket.org/account/settings/app-passwords/

import requests
import argparse

def fetch_and_save_repositories(bb_workspace, bb_user, bb_key, file_name="repositories.csv"):
    """
    Fetches repository information from Bitbucket API and writes it to a CSV file.

    Parameters:
    bb_workspace (str): Bitbucket workspace name.
    bb_user (str): Bitbucket username.
    bb_key (str): Bitbucket app password.
    file_name (str): Name of the file to save the repository information. Default is 'repositories.csv'.
    """
    bb_api_url = "https://api.bitbucket.org/2.0/repositories/"
    
    # Write the header to the file
    with open(file_name, 'w') as file:
        file.write('Project, Repository, Updated\n')

    url = bb_api_url + bb_workspace 
    headers = {"Content-Type": "application/json"}

    # Fetch and process repository data
    while url is not None:
        response = requests.get(url, headers=headers, auth=(bb_user, bb_key))

        if response.status_code == 200:
            data = response.json()
            for repo in data['values']:
                name = repo['name']
                updated = (repo['updated_on']).split('T')[0]
                project = repo['project']['key']
                print(name, project, updated)
                with open(file_name, 'a') as file:
                    file.write(f"{project}, {name}, {updated}\n")
            if 'next' in data:
                url = data['next']
            else:
                url = None
        else:
            print(response.status_code, response.text)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fetch and save Bitbucket repository information.")
    parser.add_argument("--workspace", default="dragosinc", help="Bitbucket workspace name")
    parser.add_argument("--username", required=True,  help="Bitbucket username")
    parser.add_argument("--password", required=True, help="Bitbucket app password")
    parser.add_argument("--file", default="repositories.csv", help="Output file name (default: repositories.csv)")

    args = parser.parse_args()

    fetch_and_save_repositories(args.workspace, args.username, args.password, args.file)