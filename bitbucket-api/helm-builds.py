#!/usr/bin/env python3

## List dragos-platform-helm builds

## REFS:
# https://developer.atlassian.com/cloud/bitbucket/rest/api-group-pipelines/#api-repositories-workspace-repo-slug-pipelines-get
# https://developer.atlassian.com/cloud/bitbucket/rest/intro/#filtering

import datetime
import requests
import argparse
import psycopg2

parser = argparse.ArgumentParser()
parser.add_argument("--token", help="Bitbucket Token", required=True)
# parser.add_argument("--db_user", help="Database Username", required=True)
# parser.add_argument("--db_pass", help="Database Password", required=True)
args = parser.parse_args()

db_host = "10.20.4.20"
db_name = "platformdb1"

day_ago = datetime.datetime.now() - datetime.timedelta(days=1)

page_num=1
next_page=True

headers = {"Accept": "application/json", "Authorization": "Bearer " + args.token }

## Empty list for our list of debian packages
deb_list = []
## Loop through the Bitbucket API
while next_page:
    ## Loop throuh pipeline builds
    url = "https://api.bitbucket.org/2.0/repositories/dragosinc/dragos-platform-helm/pipelines?sort=-created_on&pagelen=100&page=" + str(page_num)
    x = requests.get(url, headers=headers)
    if x.status_code == 200:
        for value in x.json()['values']:
            ## If the build finished and was successful.
            if value['state']['name'] == 'COMPLETED' and value['state']['result']['name'] == 'SUCCESSFUL':
                date_time = datetime.datetime.strptime(((value['created_on']).split("."))[0] , '%Y-%m-%dT%H:%M:%S')
                ## And if the build was completed within the last day, get the steps.
                if date_time > day_ago:
                    url2 = "https://api.bitbucket.org/2.0/repositories/dragosinc/dragos-platform-helm/pipelines/" + value['uuid'] + "/steps?pagelen=100"
                    x2 = requests.get(url2, headers=headers)
                    if x2.status_code == 200:
                        for value2 in x2.json()['values']:
                                ## And if the build had a step that was labeled 'build artifacts', get the log
                                if value2['name'] == 'Build artifacts':
                                    #print("-----------------------------------------------------------------------------------------------------")
                                    #print(value['build_number'], value['created_on'])
                                    url3 = "https://api.bitbucket.org/2.0/repositories/dragosinc/dragos-platform-helm/pipelines/" + value['uuid'] + "/steps/" + value2['uuid'] + "/log"
                                    header3 = {"Authorization": "Bearer " + args.token }
                                    #print(url3)
                                    x3 = requests.get(url3, headers=header3)
                                    if x3.status_code == 200:
                                        for line in x3.text.splitlines():
                                            ## Get the debian package urls from the logs.
                                            if line.startswith('https') and line.endswith('deb'):
                                                ## Empty list of package info
                                                list = []
                                                path = line
                                                deb_package = line.split("/")[-1]
                                                time_stamp = datetime.datetime.fromtimestamp(int(deb_package.split("-")[3].split("+")[0]))
                                                #print(path, deb_package, time_stamp)
                                                list.append(str(time_stamp))
                                                list.append(value['build_number'])
                                                list.append(deb_package)
                                                list.append(path)
                                                #print(list)
                                                deb_list.append(list)
                                    else:
                                        print(x3.status_code, x3.text)
                    else:
                        print(x.status_code, x.text)
                else:
                    next_page=False
    else:
        print(x.status_code, x.text)
        next_page=False

    page_num+=1
        


## DB Connection function
def connect():
    conn = psycopg2.connect(dbname=db_name, host=db_host, user=args.db_user, password=args.db_pass)
    return conn

print(deb_list)