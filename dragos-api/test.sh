#!/bin/bash

## Test Dragos API

user_name="admin"
pass_word="Dr@gosSyst3m"
url="https://10.20.8.103"


# Authenticate

curl -k -i -H "Content-Type: application/json" \
	-d "{'userId': $user_name, 'password': $pass_word}" \
	"$url/users/api/v3/authenticate"
