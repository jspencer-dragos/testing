#!/usr/bin/env python3

import getpass
import requests
import urllib3
import json
from base64 import b64encode

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

address = "10.20.8.101"
port = "443"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")

url_base = "https://" + address + ":" + port + "/"

# Login
print("------------------")
print("Login")
user_and_pass = user_name + ":" + pass_word
encoded = b64encode(bytes(user_and_pass, encoding='ascii')).decode('ascii')
auth_header = "Basic " + encoded
url = url_base + "users/api/v2/user/" + user_name
header = {'Content-Type': 'application/json', 'Accept': 'application/json'}
header['Authorization'] = auth_header
x = requests.get(url, headers=header, verify=False)
if x.status_code == 200:
    token = x.headers['X-Auth-Token']
    print(token)
else:
    print("Something went wrong", x.status_code, x.text)
