#!/usr/bin/env python3

import getpass
import requests
import urllib3
import json
from base64 import b64encode

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

address = "10.20.14.173"
port = "443"
user_name = input("Username: ")
pass_word = getpass.getpass("Password: ")

url_base = "https://" + address + ":" + port + "/"

# Login
print("------------------")
print("Login")
user_and_pass = user_name + ":" + pass_word
encoded = b64encode(bytes(user_and_pass, encoding='ascii')).decode('ascii')
auth_header = "Basic " + encoded
url = url_base + "users/api/v2/user/" + user_name
header = {'Content-Type': 'application/json', 'Accept': 'application/json'}
header['Authorization'] = auth_header
x = requests.get(url, headers=header, verify=False)
if x.status_code == 200:
    token = x.headers['X-Auth-Token']
    #print(token)
else:
    print("Something went wrong", x.status_code, x.text)
    exit()


# Get Notifications
print("------------------")
print("List Notifications")
header = {'X-Auth-Token': token}
url = url_base + "notifications/api/v2/notification?pageSize=100&sortField=createdAt&sortDescending=true&filter=type!=Communication;type!=System;severity=gt=0"
x = requests.get(url, headers=header, verify=False)
if x.status_code == 200:
    page = x.json()['pageNumber']
    total_pages = x.json()['totalPages']
    page_size = x.json()['pageSize']
    print("items ", page_size)
    print("Page", page, "of", total_pages)
    for item in x.json()['content']:
        id = item['id']
        created = item['createdAt']
        type = item['type']
        severity = item['severity']
        print(id, created, type, severity)
        #print(json.dumps(item, indent=4, sort_keys=True))
else:
    print("Something went wrong", x.status_code, x.text)
