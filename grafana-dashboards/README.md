# Grafana Dashboards

## How to Install

1. Go to Grafana on the Sitestore: https://sitestore/grafana/
2. On the left menu find Dashboards > Manage
3. (Optional) Create a new folder
4. Click on Import
   1. Upload the json file
   2. Pick a folder to upload to
   3. Select the data source (Prometheus)
   4. Click Import